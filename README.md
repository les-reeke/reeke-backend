# Presentation

Reeke Backend avec Spring Boot et une base de données PostgreSQL.

# Environnement de développement

## Prérequis

### Configuration

Afin de passez sur le profile Spring de développment.
Dans le fichier `src/main/resources/application.properties` remplacez `spring.profiles.active=production` par `spring.profiles.active=dev`.

### Base de données

Pour créer le schéma de la base de données, exécuter les scripts présents dans le dossier `scripts` du dépôt Git `reeke-db` (https://gitlab.com/les-reeke/reeke-db).

### Variavables d'environnement

Les variables d'environnement ci-dessous sont nécessaire :
 * jdbc.password
 * jdbc.username
 * jdbc.url

Exemple de valeur

```
jdbc.username=login
jdbc.password=password
jdbc.url=jdbc:postgresql://localhost:5432/reekedb 
```

## Exécution

Sur GNU/Linux & Mac exécutez la commande
```shell
./gradlew bootRun
```

Sur Windows exécutez la commande
```shell
./gradlew.bat bootRun
```

# Environnement de production

Dans votre `server.xml` du serveur Tomcat rajouter la ressource ci-dessous :

```xml
<Resource name="jdbc/PostgresDB" auth="Container"
    type="javax.sql.DataSource" factory="org.apache.tomcat.jdbc.pool.DataSourceFactory"
    driverClassName="org.postgresql.Driver" url="jdbc:postgresql://db:5432/reekedb"
    username="login" password="password" maxTotal="20" maxIdle="10" maxWaitMillis="-1" />
```

Dans votre `context.xml` du serveur Tomcat rajouter la ResourceLink ci-dessous :

```xml
<ResourceLink name="jdbc/PostgresDB" global="jdbc/PostgresDB" type="javax.sql.DataSource" />
```

Télécharger le JDBC de PostgresSQL ici https://repo1.maven.org/maven2/org/postgresql/postgresql/42.2.12/postgresql-42.2.12.jar.
Copiez le JAR dans le dossier `lib` du serveur Tomcat.

Récupérez le WAR à déposer dans le dossier `webapps`, le WAR est disponible sur le nexus https://nexus.partagetesco.fr/repository/maven-public/fr/iutnantes/reeke/backend/1.0.2/backend-1.0.2.war
correspondant à `fr.iutnantesµ.reeke.backend` prenez la version la plus récente (parfois plusieur WAR sont disponible avec le format `aaaaa-x` prenez celui avec le x le plus grand) et renommez le WAR en `backend.war` pour avoir une URL un peu plus user-friendly.