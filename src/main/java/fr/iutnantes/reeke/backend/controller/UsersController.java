package fr.iutnantes.reeke.backend.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.iutnantes.reeke.backend.Constants;
import fr.iutnantes.reeke.backend.entity.Allergy;
import fr.iutnantes.reeke.backend.entity.Token;
import fr.iutnantes.reeke.backend.entity.User;
import fr.iutnantes.reeke.backend.entity.UserAllergies;
import fr.iutnantes.reeke.backend.entity.api.ApiError;
import fr.iutnantes.reeke.backend.entity.api.ApiErrorSource;
import fr.iutnantes.reeke.backend.exception.InvalidCredentialsException;
import fr.iutnantes.reeke.backend.exception.InvalidTokenException;
import fr.iutnantes.reeke.backend.repository.AllergiesRepository;
import fr.iutnantes.reeke.backend.repository.NotesRepository;
import fr.iutnantes.reeke.backend.repository.UsersRepository;
import fr.iutnantes.reeke.backend.util.TokenEngine;
import org.apache.commons.codec.digest.DigestUtils;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;
import java.util.Optional;

import static java.lang.String.format;

/**
 * Controlleur des utilisateurs.
 * /user
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
@RestControllerAdvice
@RequestMapping("user")
public class UsersController extends ResponseEntityExceptionHandler {
    /**
     * Repository des utilisateurs.
     */
    private final UsersRepository usersRepository;
    /**
     * Repository des allergies.
     */
    private final AllergiesRepository allergiesRepository;
    /**
     * Repository des notes.
     */
    private final NotesRepository notesRepository;

    /**
     * Gestionnaire des tokens.
     */
    private final TokenEngine tokenEngine;

    /**
     * Constructeur appelé par Spring avec injection des dépendances.
     *
     * @param usersRepository     Le Repository des utilisateurs.
     * @param allergiesRepository Le Repository des allergies.
     * @param notesRepository     Le Repository des notes.
     * @param tokenEngine         Le gestionnaire des tokens.
     */
    @Autowired
    public UsersController(UsersRepository usersRepository, AllergiesRepository allergiesRepository,
                           NotesRepository notesRepository, TokenEngine tokenEngine) {
        this.usersRepository = usersRepository;
        this.allergiesRepository = allergiesRepository;
        this.notesRepository = notesRepository;
        this.tokenEngine = tokenEngine;
    }

    /**
     * Méthode de connexion.
     * POST /user/login
     *
     * @param login    Le login.
     * @param password Le mot de passe.
     * @return Un token.
     * @throws InvalidCredentialsException Exception levée si les informations d'identification sont invalides.
     * @throws JsonProcessingException     Exception levée si un erreur est survenu durant la sérialisation.
     * @throws JoseException               Exception levée si une erreur est survenu dans la création du JsonWebToken.
     * @throws MalformedClaimException     Exception levée si une erreur est survenu dans l'insertion des données dans le JsonWebToken.
     */
    @PostMapping(value = "login", produces = "application/json")
    public Token signIn(@RequestHeader(Constants.LOGIN_PARAM) String login,
                        @RequestHeader(Constants.PASSWORD_PARAM) String password) throws InvalidCredentialsException, JsonProcessingException, JoseException, MalformedClaimException {
        Optional<User> userFound = usersRepository.findById(login);
        if (userFound.isPresent() && userFound.get().getPassword().equals(DigestUtils.sha256Hex(password))) {
            return tokenEngine.generateToken(userFound.get());
        }

        throw new InvalidCredentialsException();
    }

    /**
     * Méthode d'inscription.
     * POST /user
     *
     * @param login     Le login.
     * @param password  Le mot de passe.
     * @param firstname Le prénom.
     * @param lastname  Le nom.
     * @param email     L'adresse mail.
     * @param home      L'emplacement de sa maison.
     * @param work      L'emplacement de son lieu de travail.
     * @return L'utilisateur créé.
     * @throws IllegalArgumentException Si le login est déjà utilisé.
     */
    @PostMapping(value = "", produces = "application/json")
    public User signUp(@RequestHeader(Constants.LOGIN_PARAM) String login,
                       @RequestHeader(Constants.PASSWORD_PARAM) String password,
                       @RequestHeader(Constants.FIRSTNAME_PARAM) String firstname,
                       @RequestHeader(Constants.LASTNAME_PARAM) String lastname,
                       @RequestHeader(Constants.EMAIL_PARAM) String email,
                       @RequestHeader(value = Constants.HOME_PARAM, required = false) String home,
                       @RequestHeader(value = Constants.WORK_PARAM, required = false) String work) {
        if (usersRepository.existsById(login)) {
            throw new IllegalArgumentException();
        }

        User user = new User(login, DigestUtils.sha256Hex(password), firstname, lastname, email);

        // TODO : Home & Work a gérer.

        usersRepository.save(user);

        return user;
    }

    /**
     * Méthode de mise à jour des informations de l'utilisateur.
     * PUT /user
     *
     * @param tokenParam Le token.
     * @param password   Le mot de passe.
     * @param firstname  Le prénom.
     * @param lastname   Le nom.
     * @param email      L'adresse mail.
     * @param home       L'emplacement de sa maison.
     * @param work       L'emplacement de son lieu de travail.
     * @return L'utilisateur à jour.
     */
    @PutMapping(value = "", produces = "application/json")
    public User updateUser(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam,
                           @RequestHeader(value = Constants.PASSWORD_PARAM, required = false) String password,
                           @RequestHeader(value = Constants.FIRSTNAME_PARAM, required = false) String firstname,
                           @RequestHeader(value = Constants.LASTNAME_PARAM, required = false) String lastname,
                           @RequestHeader(value = Constants.EMAIL_PARAM, required = false) String email,
                           @RequestHeader(value = Constants.HOME_PARAM, required = false) String home,
                           @RequestHeader(value = Constants.WORK_PARAM, required = false) String work) {
        Optional<User> userOptional = tokenEngine.readToken(tokenParam);
        if (userOptional.isEmpty()) {
            throw new InvalidTokenException();
        }
        User user = userOptional.get();

        if (password != null) {
            user.setPassword(DigestUtils.sha256Hex(password));
        }
        if (firstname != null) {
            user.setFirstname(firstname);
        }
        if (lastname != null) {
            user.setLastname(lastname);
        }
        if (email != null) {
            user.setEmail(email);
        }

        // TODO : Home & Work a gérer.

        usersRepository.save(user);

        return user;
    }

    /**
     * Méthode pour rafraichir les données du token de l'utilisateur.
     * POST /user/refreshToken
     *
     * @param tokenParam Le token.
     * @return L'utilisateur à jour.
     */
    @PostMapping(value = "refreshToken", produces = "application/json")
    public Token refreshToken(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam) throws MalformedClaimException, JoseException, JsonProcessingException {
        Optional<User> userOptional = tokenEngine.readToken(tokenParam);
        if (userOptional.isEmpty()) {
            throw new InvalidTokenException();
        }

        return tokenEngine.generateToken(userOptional.get());
    }

    /**
     * Méthode de suppression du compte.
     * DELETE /user
     *
     * @param tokenParam Le token.
     * @return L'utilisateur supprimé.
     */
    @DeleteMapping(value = "", produces = "application/json")
    public User deleteUser(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam) {
        Optional<User> userOptional = tokenEngine.readToken(tokenParam);
        if (userOptional.isEmpty()) {
            throw new InvalidTokenException();
        }

        // Suppression des données rattachées.
        User user = userOptional.get();
        user.setAllergies(Collections.emptySet());
        usersRepository.save(user);
        notesRepository.deleteByUser(user);
        usersRepository.delete(user);

        return user;
    }

    /**
     * Récupérer la liste des allergies de l'utilisateur.
     * GET /user/allergies
     *
     * @param tokenParam Le token.
     * @return Les allergies de l'utilisateur
     */
    @GetMapping(value = "allergies", produces = "application/json")
    public UserAllergies getAllergies(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam) {
        Optional<User> userOptional = tokenEngine.readToken(tokenParam);
        if (userOptional.isEmpty()) {
            throw new InvalidTokenException();
        }

        return new UserAllergies(userOptional.get());
    }

    /**
     * Mettre à jour la liste des allergies de l'utilisateur.
     * POST /user/allergies
     *
     * @param tokenParam     Le token.
     * @param allergiesParam Les allergies (format: allergie1,allergie2, allergie3).
     * @return Les allergies de l'utilisateur
     */
    @PostMapping(value = "allergies", produces = "application/json")
    public UserAllergies postAllergies(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam,
                                       @RequestHeader(value = Constants.ALLERGIES_PARAM, required = false) String allergiesParam) {
        Optional<User> userOptional = tokenEngine.readToken(tokenParam);
        if (userOptional.isEmpty()) {
            throw new InvalidTokenException();
        }
        User user = userOptional.get();

        allergiesParam = allergiesParam.trim().replaceAll(",[ ]*", ",");
        String[] allergies = allergiesParam.split(",");

        for (String allergyName : allergies) {
            Optional<Allergy> allergyOptional = allergiesRepository.findOneByNameIgnoreCase(allergyName);
            allergyOptional.ifPresent(user::addAllergy);

            if (allergyOptional.isEmpty()) {
                Allergy allergy = new Allergy(allergyName);
                allergiesRepository.save(allergy);
                user.addAllergy(allergy);
            }
        }
        usersRepository.save(user);

        return new UserAllergies(user);
    }

    /**
     * Mettre à jour la liste des allergies de l'utilisateur.
     * DELETE /user/allergies
     *
     * @param tokenParam     Le token.
     * @param allergiesParam Les allergies (format: allergie1,allergie2, allergie3).
     * @return Les allergies de l'utilisateur
     */
    @DeleteMapping(value = "allergies", produces = "application/json")
    public UserAllergies deleteAllergies(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam,
                                         @RequestHeader(value = Constants.ALLERGIES_PARAM) String allergiesParam) {
        Optional<User> userOptional = tokenEngine.readToken(tokenParam);
        if (userOptional.isEmpty()) {
            throw new InvalidTokenException();
        }
        User user = userOptional.get();

        allergiesParam = allergiesParam.trim().replaceAll(",[ ]*", ",");
        String finalAllergiesParam = allergiesParam;

        user.getAllergies().removeIf(userAllergy -> !finalAllergiesParam.toLowerCase().contains(userAllergy.getName()));
        usersRepository.save(user);

        return new UserAllergies(user);
    }

    /**
     * Gérer les paramètres manquant dans les en-têtes HTTP.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({MissingRequestHeaderException.class})
    protected ResponseEntity<Object> handleMissingRequestHeader(MissingRequestHeaderException ex, ServletWebRequest request) {
        ApiError apiError;
        switch (ex.getHeaderName()) {
            case Constants.TOKEN_PARAM:
                apiError = new ApiError(
                        "400000", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400000", "TOKEN REQUIRED", "Token attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName())
                );
                break;
            case Constants.LOGIN_PARAM:
                apiError = new ApiError(
                        "400001", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400001", "LOGIN REQUIRED", "Login attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName())
                );
                break;
            case Constants.PASSWORD_PARAM:
                apiError = new ApiError(
                        "400002", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400002", "PASSWORD REQUIRED", "Password attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName())
                );
                break;
            case Constants.FIRSTNAME_PARAM:
                apiError = new ApiError(
                        "400003", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400003", "FIRSTNAME REQUIRED", "Firstname attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName())
                );
                break;
            case Constants.LASTNAME_PARAM:
                apiError = new ApiError(
                        "400004", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400004", "LASTNAME REQUIRED", "Lastname attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName())
                );
                break;
            case Constants.EMAIL_PARAM:
                apiError = new ApiError(
                        "400005", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400005", "EMAIL REQUIRED", "Email attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName())
                );
                break;
            default:
                apiError = new ApiError(
                        "400006", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400006", "DEPENDENT PARAMETER REQUIRED", format("%s need %s to be given", ex.getHeaderName(), "undefined"),
                        new ApiErrorSource(request.getRequest().getRequestURI(), "login")
                );
                break;
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiError);
    }

    /**
     * Gérer les conflits.
     *
     * @param ex      L'exception.
     * @param request La requête qui génère un conflit.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, ServletWebRequest request) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(
                new ApiError(
                        "409001", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/409", "409 CONFLICT",
                        "409001", "ID NOT UNIQUE", "Login must be unique",
                        new ApiErrorSource(request.getRequest().getRequestURI(), "login")
                ));
    }

    /**
     * Gérer les authentification échoués.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({InvalidCredentialsException.class})
    protected ResponseEntity<Object> handleInvalidCredentials(RuntimeException ex, ServletWebRequest request) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                new ApiError(
                        "401000", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/401", "401 UNAUTHORIZED",
                        "401000", "AUTHENTICATION FAILED", "Invalid credentials",
                        new ApiErrorSource(request.getRequest().getRequestURI())
                ));
    }

    /**
     * Gérer les tokens invalides dans les en-têtes HTTP.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({InvalidTokenException.class})
    protected ResponseEntity<Object> handleInvalidToken(InvalidTokenException ex, ServletWebRequest request) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ApiError(
                "401001", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/401", "401 UNAUTHORIZED",
                "401001", "INVALID TOKEN", "Token is invalid",
                new ApiErrorSource(request.getRequest().getRequestURI(), "token")));
    }
}
