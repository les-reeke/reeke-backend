package fr.iutnantes.reeke.backend.controller;

import fr.iutnantes.reeke.backend.Constants;
import fr.iutnantes.reeke.backend.entity.Restaurant;
import fr.iutnantes.reeke.backend.entity.api.PageResponse;
import fr.iutnantes.reeke.backend.repository.RestaurantsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.Time;

import javax.servlet.http.HttpServletRequest;

/**
 * @author JRoul
 * @version 0.1
 */
@RestControllerAdvice
@RequestMapping("restaurants")
public class RestaurantsController extends ResponseEntityExceptionHandler {

    @Autowired
    RestaurantsRepository restaurantsRepository;

    @GetMapping(value = "", produces = "application/json")
    public PageResponse<Restaurant> getAllRestaurant(
            @RequestParam(value = Constants.FILTER_RESTAURANT_ID, required = false) Integer restaurantId,
            @RequestParam(value = Constants.FILTER_RESTAURANT_NAME, required = false) String restaurantName,
            @RequestParam(value = Constants.FILTER_RESTAURANT_BEGIN, required = false) Time restaurantBegin,
            @RequestParam(value = Constants.FILTER_RESTAURANT_END, required = false) Time restaurantEnd,
            @RequestParam(value = Constants.FILTER_RESTAURANT_NOTE, required = false) Double restaurantNote,
            @RequestParam(value = Constants.FILTER_RESTAURANT_PLACE, required = false) String restaurantPlace,
            @RequestParam(value = Constants.FILTER_RESTAURANT_DISH, required = false) String restaurantDish,
            @RequestParam(value = Constants.SORT_PARAM, required = false) String sortParam,
            @RequestParam(value = Constants.PAGE_NUMBER_PARAM, required = false, defaultValue = "0") Integer page,
            @RequestParam(value = Constants.PAGE_SIZE_PARAM, required = false, defaultValue = "20") Integer pageSize,
            HttpServletRequest request) {
        
        String param = "";
        if (sortParam != null){
            sortParam = sortParam.trim().replaceAll(",[ ]*", ",");
            String[] sortAttributeNames = sortParam.split(",");
            param = sortAttributeNames[0];
        }

        if(restaurantId != null){
            if(param.equals("id")){
                return new PageResponse<>(restaurantsRepository.findAllByIdOrderByIdAsc(restaurantId,PageRequest.of(page, pageSize)), request);
            }else{
                return new PageResponse<>(restaurantsRepository.findOneById(restaurantId,PageRequest.of(page, pageSize)), request); 
            }
        }else if(restaurantName != null){
            if(param.equals("note")){
                return new PageResponse<>(restaurantsRepository.findAllByNameContainingIgnoreCaseOrderByNameAsc(restaurantName,PageRequest.of(page, pageSize)), request);
            }else{
                return new PageResponse<>(restaurantsRepository.findOneByNameContainingIgnoreCase(restaurantName,PageRequest.of(page, pageSize)), request);
            }
        }else if(restaurantBegin != null){
            return new PageResponse<>(restaurantsRepository.findByBegin(restaurantBegin, PageRequest.of(page, pageSize)), request);
        }else if(restaurantEnd != null){
            return new PageResponse<>(restaurantsRepository.findByEnd(restaurantEnd, PageRequest.of(page, pageSize)), request);
        }else if(restaurantNote != null){
            if(param.equals("note")){
                return new PageResponse<>(restaurantsRepository.findAllByNoteOrderByNoteAsc(restaurantNote,PageRequest.of(page, pageSize)), request);
            }else{
                return new PageResponse<>(restaurantsRepository.findAllByNote(restaurantNote,PageRequest.of(page, pageSize)), request);
            }
        }else if(restaurantPlace != null){
            return new PageResponse<>(restaurantsRepository.findAll(PageRequest.of(page, pageSize)), request);
        }else if(restaurantDish != null){
            return new PageResponse<>(restaurantsRepository.findAll(PageRequest.of(page, pageSize)), request);
        }else{
            if(param.equals("id")){
                return new PageResponse<>(restaurantsRepository.findAllByOrderByIdAsc(PageRequest.of(page, pageSize)), request);
            }else if(param.equals("name")){
                return new PageResponse<>(restaurantsRepository.findAllByOrderByNameAsc(PageRequest.of(page, pageSize)), request);
            }else if(param.equals("note")){
                return new PageResponse<>(restaurantsRepository.findAllByOrderByNoteAsc(PageRequest.of(page, pageSize)), request);
            }else{
                return new PageResponse<>(restaurantsRepository.findAll(PageRequest.of(page, pageSize)), request);
            }
        }
    }
}