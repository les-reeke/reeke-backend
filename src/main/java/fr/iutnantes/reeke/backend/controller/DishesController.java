package fr.iutnantes.reeke.backend.controller;

import fr.iutnantes.reeke.backend.Constants;
import fr.iutnantes.reeke.backend.entity.*;
import fr.iutnantes.reeke.backend.entity.api.ApiError;
import fr.iutnantes.reeke.backend.entity.api.ApiErrorSource;
import fr.iutnantes.reeke.backend.entity.api.PageResponse;
import fr.iutnantes.reeke.backend.entity.note.Note;
import fr.iutnantes.reeke.backend.exception.InvalidCategoryException;
import fr.iutnantes.reeke.backend.exception.InvalidCredentialsException;
import fr.iutnantes.reeke.backend.exception.InvalidDishException;
import fr.iutnantes.reeke.backend.exception.InvalidRestaurantException;
import fr.iutnantes.reeke.backend.repository.*;
import fr.iutnantes.reeke.backend.util.TokenEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * Controlleur des plats.
 * /restaurants/dishes
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
@RestControllerAdvice
@RequestMapping("restaurants/dishes")
public class DishesController extends ResponseEntityExceptionHandler {
    /**
     * Le repository des restaurants.
     */
    private final RestaurantsRepository restaurantsRepository;
    /**
     * Le repository des plats.
     */
    private final DishesRepository dishesRepository;
    /**
     * Le repository des catégories.
     */
    private final CategoriesRepository categoriesRepository;
    /**
     * Le repository des notes.
     */
    private final NotesRepository notesRepository;
    /**
     * Repository des allergies.
     */
    private final AllergiesRepository allergiesRepository;
    /**
     * Repository des images.
     */
    private final ImagesRepository imagesRepository;
    /**
     * Gestionnaire des tokens.
     */
    private final TokenEngine tokenEngine;

    /**
     * Constructeur par défaut appelé par Spring pour l'injection de dépendance.
     *
     * @param dishesRepository      Le repository des plats.
     * @param restaurantsRepository Le repository des restaurants.
     * @param categoriesRepository  Le repository des catégories.
     * @param notesRepository       Le repository des notes.
     * @param allergiesRepository   Le repository des allergies.
     * @param imagesRepository      Le repository des images.
     * @param tokenEngine           Le gestionnaire de tokens.
     */
    @Autowired
    public DishesController(RestaurantsRepository restaurantsRepository, DishesRepository dishesRepository,
                            CategoriesRepository categoriesRepository, NotesRepository notesRepository,
                            AllergiesRepository allergiesRepository, ImagesRepository imagesRepository, TokenEngine tokenEngine) {
        this.restaurantsRepository = restaurantsRepository;
        this.dishesRepository = dishesRepository;
        this.categoriesRepository = categoriesRepository;
        this.notesRepository = notesRepository;
        this.allergiesRepository = allergiesRepository;
        this.imagesRepository = imagesRepository;
        this.tokenEngine = tokenEngine;
    }

    /**
     * Créer un plat dans un restaurant.
     * POST /restaurants/dishes
     *
     * @param tokenParam     Le token.
     * @param restaurantId   L'id du restaurant.
     * @param name           Le nom.
     * @param categoryName   L'id de la catégorie.
     * @param description    La description.
     * @param price          Le prix.
     * @param noteParam      La note.
     * @param allergiesParam Les allergies (format: allergie1,allergie2, allergie3).
     * @param imagesParam    Les images (format: url1, url2,  url3).
     * @return Le plat.
     */
    @PostMapping(value = "", produces = "application/json")
    public Dish postDish(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam,
                         @RequestHeader(value = Constants.RESTAURANT_PARAM) Integer restaurantId,
                         @RequestHeader(value = Constants.CATEGORY_PARAM) String categoryName,
                         @RequestHeader(value = Constants.NAME_PARAM) String name,
                         @RequestHeader(value = Constants.DESCRIPTION_PARAM, required = false) String description,
                         @RequestHeader(value = Constants.PRICE_PARAM, required = false) Double price,
                         @RequestHeader(value = Constants.NOTE_PARAM, required = false) Double noteParam,
                         @RequestHeader(value = Constants.ALLERGIES_PARAM, required = false) String allergiesParam,
                         @RequestHeader(value = Constants.IMAGES_PARAM, required = false) String imagesParam) {
        Optional<Restaurant> restaurantOptional = restaurantsRepository.findById(restaurantId);
        Optional<Category> category = categoriesRepository.findOneByNameIgnoreCase(categoryName);

        Optional<User> user = tokenEngine.readToken(tokenParam);
        if (user.isEmpty()) {
            throw new InvalidCredentialsException();
        }

        if (category.isEmpty()) {
            Category newCategory = new Category(categoryName);
            categoriesRepository.save(newCategory);
            category = Optional.of(newCategory);
        }

        if (restaurantOptional.isEmpty()) {
            throw new InvalidRestaurantException();
        }

        Dish dish = new Dish(name, description, price, category.get());
        if (allergiesParam != null) {
            addAllergiesToDish(allergiesParam, dish);
        }
        if (imagesParam != null) {
            addImagesToDish(imagesParam, dish);
        }
        dishesRepository.save(dish);
        Restaurant restaurant = restaurantOptional.get();
        if (noteParam != null) {
            Note note = new Note(user.get(), restaurant, dish, noteParam);
            notesRepository.save(note);
            dish.addNote(note);
            dishesRepository.save(dish);
        }
        restaurant.addDish(dish);
        restaurantsRepository.save(restaurant);

        return dish;
    }

    /**
     * Ajout des allergies au plat.
     *
     * @param allergiesParam Liste des allergies (format allergie1,allergie2, allergie3).
     * @param dish           Le plat.
     */
    private void addAllergiesToDish(String allergiesParam, Dish dish) {
        allergiesParam = allergiesParam.trim().replaceAll(",[ ]*", ",");
        String[] allergies = allergiesParam.split(",");

        for (String allergyName : allergies) {
            Optional<Allergy> allergyOptional = allergiesRepository.findOneByNameIgnoreCase(allergyName);
            allergyOptional.ifPresent(dish::addAllergy);

            if (allergyOptional.isEmpty()) {
                Allergy allergy = new Allergy(allergyName);
                allergiesRepository.save(allergy);
                dish.addAllergy(allergy);
            }
        }
    }

    /**
     * Ajout des images au plat.
     *
     * @param imagesParam Liste des images (format url1, url2,  url3).
     * @param dish        Le plat.
     */
    private void addImagesToDish(String imagesParam, Dish dish) {
        imagesParam = imagesParam.trim().replaceAll(",[ ]*", ", ");
        String[] images = imagesParam.split(", ");

        for (String imageURL : images) {
            Optional<Image> imageOptional = imagesRepository.findOneByUrl(imageURL);
            imageOptional.ifPresent(dish::addImage);

            if (imageOptional.isEmpty()) {
                Image image = new Image(imageURL);
                imagesRepository.save(image);
                dish.addImage(image);
            }
        }
    }

    /**
     * Modifier un plat dans un restaurant.
     * PUT /restaurants/dishes
     *
     * @param tokenParam     Le token.
     * @param restaurantId   L'id du restaurant.
     * @param name           Le nom.
     * @param categoryName   L'id de la catégorie.
     * @param description    La description.
     * @param price          Le prix.
     * @param noteParam      La note.
     * @param allergiesParam Les allergies (format: allergie1,allergie2, allergie3).
     * @param imagesParam    Les images (format: url1, url2,  url3).
     * @return Le plat.
     */
    @PutMapping(value = "", produces = "application/json")
    public Dish putDish(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam,
                        @RequestHeader(value = Constants.ID_PARAM) Integer dishId,
                        @RequestHeader(value = Constants.RESTAURANT_PARAM) Integer restaurantId,
                        @RequestHeader(value = Constants.CATEGORY_PARAM, required = false) String categoryName,
                        @RequestHeader(value = Constants.NAME_PARAM, required = false) String name,
                        @RequestHeader(value = Constants.DESCRIPTION_PARAM, required = false) String description,
                        @RequestHeader(value = Constants.PRICE_PARAM, required = false) Double price,
                        @RequestHeader(value = Constants.NOTE_PARAM, required = false) Double noteParam,
                        @RequestHeader(value = Constants.ALLERGIES_PARAM, required = false) String allergiesParam,
                        @RequestHeader(value = Constants.IMAGES_PARAM, required = false) String imagesParam) {
        Optional<User> user = tokenEngine.readToken(tokenParam);
        if (user.isEmpty()) {
            throw new InvalidCredentialsException();
        }

        Optional<Restaurant> restaurantOptional = restaurantsRepository.findById(restaurantId);
        if (restaurantOptional.isEmpty()) {
            throw new InvalidRestaurantException();
        }
        Optional<Dish> dishOptional = dishesRepository.findById(dishId);
        if (dishOptional.isEmpty()) {
            throw new InvalidDishException();
        }

        Dish dish = dishOptional.get();
        if (categoryName != null) {
            Optional<Category> category = categoriesRepository.findOneByNameIgnoreCase(categoryName);
            if (category.isEmpty()) {
                Category newCategory = new Category(categoryName);
                categoriesRepository.save(newCategory);
                dish.setCategory(newCategory);
            } else {
                dish.setCategory(category.get());
            }
        }
        if (name != null) {
            dish.setName(name);
        }
        if (description != null) {
            dish.setDescription(description);
        }
        if (price != null) {
            dish.setPrice(price);
        }
        if (noteParam != null) {
            Optional<Note> noteOptional = notesRepository.findOneByUserAndDish(user.get(), dish);
            if (noteOptional.isEmpty()) {
                Note note = new Note(user.get(), restaurantOptional.get(), dish, noteParam);
                notesRepository.save(note);
                dish.addNote(note);
            } else {
                Note note = noteOptional.get();
                note.setNote(noteParam);
                notesRepository.save(note);
            }
        }
        if (allergiesParam != null) {
            addAllergiesToDish(allergiesParam, dish);
        }
        if (imagesParam != null) {
            addImagesToDish(imagesParam, dish);
        }
        dishesRepository.save(dish);

        return dish;
    }

    /**
     * Supprimer un/des plat(s) dans un restaurant.
     * DELETE /restaurants/dishes
     *
     * @param restaurantId   L'id du restaurant.
     * @param name           Le nom.
     * @param noteParam      La note.
     * @param allergiesParam Les allergies (format: allergie1,allergie2, allergie3).
     * @return Le plat.
     */
    @DeleteMapping(value = "", produces = "application/json")
    public Dish deleteDish(@RequestHeader(value = Constants.TOKEN_PARAM) String tokenParam,
                           @RequestParam(value = Constants.ID_PARAM) Integer dishId,
                           @RequestParam(value = Constants.RESTAURANT_PARAM, required = false) Integer restaurantId,
                           @RequestParam(value = Constants.NAME_PARAM, required = false) String name,
                           @RequestParam(value = Constants.NOTE_PARAM, required = false) Double noteParam,
                           @RequestParam(value = Constants.ALLERGIES_PARAM, required = false) String allergiesParam) {
        Optional<User> user = tokenEngine.readToken(tokenParam);
        if (user.isEmpty()) {
            throw new InvalidCredentialsException();
        }

        Optional<Dish> dishOptional = dishesRepository.findById(dishId);
        if (dishOptional.isEmpty()) {
            throw new InvalidDishException();
        }

        Dish dish = dishOptional.get();
        // Suppression du plat du restaurant.
        Optional<Restaurant> restaurantOptional = restaurantsRepository.findOneByDishes(dish);
        if (restaurantOptional.isPresent()) {
            Restaurant restaurant = restaurantOptional.get();
            restaurant.removeDish(dish);
            restaurantsRepository.save(restaurant);
        }
        for (Note note : dish.getNotes()) {
            notesRepository.delete(note);
        }
        dishesRepository.delete(dish);

        return dish;
    }

    /**
     * Récupération des plats.
     * GET /restaurants/dishes
     *
     * @param restaurantId   L'id du restaurant.
     * @param page           La page à récupérer par défaut 0.
     * @param pageSize       La taille de la page par défaut 20.
     * @param request        La requête HTTP (injecté par Spring).
     * @param allergiesParam Les allergies (format: allergie1,allergie2, allergie3).
     * @param sortParam      Les tries dans l'ordre (format: trie1,trie2, trie3).
     * @return Les plats.
     */
    @GetMapping(value = "", produces = "application/json")
    public PageResponse<Dish> getDishes(@RequestParam(value = Constants.FILTER_RESTAURANT_PARAM) Integer restaurantId,
                                        @RequestParam(value = Constants.FILTER_ALLERGIES_PARAM, required = false) String allergiesParam,
                                        @RequestParam(value = Constants.FILTER_NAME_PARAM, required = false) String nameParam,
                                        @RequestParam(value = Constants.SORT_PARAM, required = false) String sortParam,
                                        @RequestParam(value = Constants.PAGE_NUMBER_PARAM, required = false, defaultValue = "0") Integer page,
                                        @RequestParam(value = Constants.PAGE_SIZE_PARAM, required = false, defaultValue = "20") Integer pageSize,
                                        HttpServletRequest request) {
        Optional<Restaurant> restaurant = restaurantsRepository.findById(restaurantId);

        if (restaurant.isEmpty()) {
            throw new InvalidRestaurantException();
        }

        Pageable pageable;
        if (sortParam != null) {
            sortParam = sortParam.trim().replaceAll(",[ ]*", ",");
            String[] sortAttributeNames = sortParam.split(",");

            Sort sort = Sort.unsorted();
            for (String sortAttributeName : sortAttributeNames) {
                sort = sort.and(Sort.by(Sort.Direction.ASC, sortAttributeName));
            }
            pageable = PageRequest.of(page, pageSize, sort);
        } else {
            pageable = PageRequest.of(page, pageSize);
        }

        Page<Dish> dishesPage = Page.empty();
        if (allergiesParam != null && !allergiesParam.isEmpty()) {
            allergiesParam = allergiesParam.trim().replaceAll(",[ ]*", ",");
            String[] allergiesNames = allergiesParam.split(",");

            List<String> allergiesRealNames = Arrays.stream(allergiesNames)
                    .map(allergyName -> {
                        Optional<Allergy> allergyOptional = allergiesRepository.findOneByNameIgnoreCase(allergyName);
                        if (allergyOptional.isPresent()) {
                            return allergyOptional.get();
                        }

                        Allergy allergy = new Allergy(allergyName);
                        allergiesRepository.save(allergy);
                        return allergy;
                    })
                    .map(Allergy::getName)
                    .collect(Collectors.toList());

            if (nameParam != null) {
                dishesPage = dishesRepository.findAllByRestaurantWithoutAllergiesAndNameContainingIgnoreCase(restaurant.get().getId(),
                        allergiesRealNames, nameParam, pageable);
            } else {
                dishesPage = dishesRepository.findAllByRestaurantWithoutAllergies(restaurant.get().getId(), allergiesRealNames, pageable);
            }
        }
        if (allergiesParam == null && nameParam != null && !nameParam.isEmpty()) {
            dishesPage = dishesRepository.findAllByRestaurantsAndNameContainingIgnoreCase(restaurant.get(), nameParam, pageable);
        }

        if ((allergiesParam == null || allergiesParam.isEmpty()) && (nameParam == null || nameParam.isEmpty())) {
            dishesPage = dishesRepository.findByRestaurants(restaurant.get(), pageable);
        }

        PageResponse<Dish> pageResponse = new PageResponse<>(dishesPage, request);

        restaurant.ifPresent(value -> pageResponse.setRelated(new PageResponse<>(
                new PageImpl<>(Collections.singletonList(value), PageRequest.of(page, pageSize), 1),
                request.getRequestURL().toString()
                        .replace(request.getRequestURI(), String.format("/restaurants?filter%%5Bid%%5D=%d", restaurantId)))));

        return pageResponse;
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError apiError;
        switch (ex.getParameterName()) {
            case Constants.NAME_PARAM:
                apiError = new ApiError(
                        "400008", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400008", "NAME REQUIRED", "Name attempted to be in URL",
                        new ApiErrorSource(((ServletWebRequest) request).getRequest().getRequestURI(), ex.getParameterName()));
                break;
            case Constants.RESTAURANT_PARAM:
                apiError = new ApiError(
                        "400013", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400013", "RESTAURANT REQUIRED", "Restaurant attempted to be in URL",
                        new ApiErrorSource(((ServletWebRequest) request).getRequest().getRequestURI(), ex.getParameterName()));
                break;
            case Constants.FILTER_RESTAURANT_PARAM:
                apiError = new ApiError(
                        "400007", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400007", "RESTAURANT REQUIRED", "Restaurant attempted to be in URL",
                        new ApiErrorSource(((ServletWebRequest) request).getRequest().getRequestURI(), ex.getParameterName())
                );
                break;
            default:
                apiError = new ApiError(
                        "400006", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400006", "DEPENDENT PARAMETER REQUIRED", format("%s need %s to be given", ex.getParameterName(), "undefined"),
                        new ApiErrorSource(((ServletWebRequest) request).getRequest().getRequestURI(), "name"));
                break;
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiError);
    }

    /**
     * Gérer les identifiants invalides dans les en-têtes HTTP.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({IllegalArgumentException.class})
    protected ResponseEntity<Object> handleIllegalArgument(IllegalArgumentException ex, ServletWebRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(
                "400011", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                "400011", "ARGUMENT INVALID", ex.getMessage(),
                new ApiErrorSource(request.getRequest().getRequestURI(), ex.getMessage().split(" ")[0].toLowerCase())));
    }

    /**
     * Gérer les paramètres manquant dans les en-têtes HTTP.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({MissingRequestHeaderException.class})
    protected ResponseEntity<Object> handleMissingRequestHeader(MissingRequestHeaderException ex, ServletWebRequest request) {
        ApiError apiError;
        switch (ex.getHeaderName()) {
            case Constants.TOKEN_PARAM:
                apiError = new ApiError(
                        "400000", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400000", "TOKEN REQUIRED", "Token attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName()));
                break;
            case Constants.NAME_PARAM:
                apiError = new ApiError(
                        "400008", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400008", "NAME REQUIRED", "Name attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName()));
                break;
            case Constants.RESTAURANT_PARAM:
                apiError = new ApiError(
                        "400013", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400013", "RESTAURANT REQUIRED", "Restaurant attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName()));
                break;
            case Constants.CATEGORY_PARAM:
                apiError = new ApiError(
                        "400014", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400014", "CATEGORY REQUIRED", "Category attempted to be in request header",
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName()));
                break;
            default:
                apiError = new ApiError(
                        "400006", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                        "400006", "DEPENDENT PARAMETER REQUIRED", format("%s need %s to be given", ex.getHeaderName(), "undefined"),
                        new ApiErrorSource(request.getRequest().getRequestURI(), ex.getHeaderName()));
                break;
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiError);
    }

    /**
     * Gérer les identifiants invalides dans les en-têtes HTTP.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({InvalidCredentialsException.class})
    protected ResponseEntity<Object> handleInvalidCredentials(InvalidCredentialsException ex, ServletWebRequest request) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ApiError(
                "401001", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/401", "401 UNAUTHORIZED",
                "401001", "INVALID TOKEN", "Token is invalid",
                new ApiErrorSource(request.getRequest().getRequestURI(), "token")));
    }

    /**
     * Gérer les restaurants invalides dans les en-têtes HTTP.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({InvalidRestaurantException.class})
    protected ResponseEntity<Object> handleInvalidRestaurant(InvalidRestaurantException ex, ServletWebRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(
                "400009", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                "400009", "RESTAURANT INVALID", "Restaurant doesn't exist",
                new ApiErrorSource(request.getRequest().getRequestURI(), Constants.RESTAURANT_PARAM)));
    }

    /**
     * Gérer les catégories invalides dans les en-têtes HTTP.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({InvalidCategoryException.class})
    protected ResponseEntity<Object> handleInvalidCategory(InvalidCategoryException ex, ServletWebRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(
                "400010", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                "400010", "CATEGORY INVALID", "Category doesn't exist",
                new ApiErrorSource(request.getRequest().getRequestURI(), Constants.CATEGORY_PARAM)));
    }

    /**
     * Gérer les plats invalides dans les en-têtes HTTP.
     *
     * @param ex      L'exception.
     * @param request La requête.
     * @return Une réponse HTTP.
     */
    @ExceptionHandler({InvalidDishException.class})
    protected ResponseEntity<Object> handleInvalidDish(InvalidDishException ex, ServletWebRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError(
                "400012", "https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400", "400 BAD REQUEST",
                "400012", "DISH INVALID", "Dish is invalid",
                new ApiErrorSource(request.getRequest().getRequestURI(), Constants.ID_PARAM)));
    }
}
