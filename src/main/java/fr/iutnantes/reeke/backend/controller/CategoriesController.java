package fr.iutnantes.reeke.backend.controller;

import fr.iutnantes.reeke.backend.entity.Category;
import fr.iutnantes.reeke.backend.entity.api.PageResponse;
import fr.iutnantes.reeke.backend.repository.CategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Controlleur pour les catégories.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
 */
@RestControllerAdvice
@RequestMapping(value = "categories")
public class CategoriesController extends ResponseEntityExceptionHandler {
    /**
     * Repository des catégories.
     */
    @Autowired
    private CategoriesRepository categoriesRepository;

    /**
     * Récupérer la liste des catégories.
     * GET /categories
     *
     * @param request La requête HTTP.
     * @return Une page contenant toutes les catégories.
     */
    @GetMapping(value = "", produces = "application/json")
    public PageResponse<Category> getCategories(HttpServletRequest request) {
        return new PageResponse<>(categoriesRepository.findAll(PageRequest.of(0, Integer.MAX_VALUE)), request);
    }
}
