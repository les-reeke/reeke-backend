package fr.iutnantes.reeke.backend.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import fr.iutnantes.reeke.backend.entity.User;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

/**
 * User JSON Serializer au format JSON Api.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see com.fasterxml.jackson.databind.JsonSerializer
 */
@JsonComponent
public class UserJsonSerializer extends JsonSerializer<User> {
    @Override
    public void serialize(User value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        CommonJsonSerializer.startSerializing(gen);

        gen.writeObjectFieldStart("data");

        gen.writeStringField("id", value.getLogin());
        gen.writeStringField("type", "user");

        gen.writeObjectFieldStart("attributes");
        gen.writeObjectField("firstname", value.getFirstname());
        gen.writeObjectField("lastname", value.getLastname());
        gen.writeObjectField("email", value.getEmail());
        gen.writeEndObject();

        gen.writeEndObject();

        CommonJsonSerializer.finishSerializing(gen);
    }
}
