package fr.iutnantes.reeke.backend.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import fr.iutnantes.reeke.backend.entity.api.ApiError;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.util.Collections;

/**
 * Serializer pour les erreurs API.
 *
 * @author Thibaut PICHON.
 * @version 0.1
 */
@JsonComponent
public class ApiErrorDataSerializer extends JsonSerializer<ApiError> {
    @Override
    public void serialize(ApiError value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        CommonJsonSerializer.startSerializing(gen);
        gen.writeObjectField("data", null);
        CommonJsonSerializer.finishSerializing(gen, Collections.singletonList(value));
    }
}
