package fr.iutnantes.reeke.backend.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import fr.iutnantes.reeke.backend.entity.Dish;
import fr.iutnantes.reeke.backend.entity.Image;
import fr.iutnantes.reeke.backend.entity.OpenedTime;
import fr.iutnantes.reeke.backend.entity.Restaurant;
import fr.iutnantes.reeke.backend.entity.api.PageResponse;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

/**
 * Page JSON Serializer.
 *
 * @author Jocelyn ROUL & Thibaut PICHON
 * @version 0.1
 */
@JsonComponent
public class PageJsonSerializer extends JsonSerializer<PageResponse<?>> {
    @Override
    public void serialize(PageResponse<?> page, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        CommonJsonSerializer.startSerializing(gen);

        gen.writeArrayFieldStart("data");

        for (Object obj : page) {
            gen.writeStartObject();

            if (obj instanceof Dish) {
                DishJsonSerializer.serializeDish((Dish) obj, gen);
            } else if (obj instanceof Restaurant) {
                serializeRestaurant((Restaurant) obj, gen);
            }

            gen.writeEndObject();
        }
        gen.writeEndArray();

        writeLinks(page, gen);

        CommonJsonSerializer.finishSerializing(gen);
    }

    /**
     * Écrire la partie des liens de navigation pour les pages.
     *
     * @param page La page courante.
     * @param gen  Le générateur de JSON.
     * @throws IOException Exception.
     */
    private void writeLinks(PageResponse<?> page, JsonGenerator gen) throws IOException {
        gen.writeObjectFieldStart("links");

        if (page.getRelated() != null) {
            gen.writeObjectFieldStart("related");
            gen.writeObjectField("href", page.getRelated().getUrl());
            gen.writeObjectFieldStart("meta");
            gen.writeObjectField("count", page.getRelated().getTotalElements());
            gen.writeEndObject();
            gen.writeEndObject();
        }

        gen.writeObjectField("first", page.getFirstURL());
        gen.writeObjectField("last", page.getLastURL());
        gen.writeObjectField("next", page.getNextURL());
        gen.writeObjectField("prev", page.getPreviousURL());

        gen.writeEndObject();
    }

    /**
     * Serialiser le {@link Restaurant}.
     *
     * @param restaurant Le restaurant.
     * @param gen        Le générateur de JSON.
     * @throws IOException Exception.
     */
    private void serializeRestaurant(Restaurant restaurant, JsonGenerator gen) throws IOException {
        gen.writeNumberField("id", restaurant.getId());
        gen.writeStringField("type", "user");

        gen.writeObjectFieldStart("attributes");
        gen.writeObjectField("name", restaurant.getName());
        gen.writeObjectField("country", restaurant.getPlace().getCountry());
        gen.writeObjectField("department", restaurant.getPlace().getDepartment());
        gen.writeObjectField("municipality", restaurant.getPlace().getMunicipality());
        gen.writeObjectField("tel", restaurant.getTel());
        gen.writeObjectField("mail", restaurant.getMail());
        gen.writeObjectField("web", restaurant.getWeb());
        gen.writeObjectField("postalcode", restaurant.getPlace().getPostalCode());
        gen.writeObjectField("municipalityinseecode", restaurant.getPlace().getMunicipalityInseeCode());
        gen.writeObjectField("category", restaurant.getCategory().getName());

        gen.writeArrayFieldStart("images");
            for (Image image : restaurant.getImages()) {
                gen.writeStartObject();
                gen.writeObjectField("url", image.getUrl());
                gen.writeEndObject();
            }
        gen.writeEndArray();

        gen.writeObjectField("address", restaurant.getPlace().getAdress());
        gen.writeObjectField("latitude", restaurant.getPlace().getLatitude());
        gen.writeObjectField("longitude", restaurant.getPlace().getLongitude());

        gen.writeArrayFieldStart("open");
        
        for(OpenedTime openedTime : restaurant.getOpenedTimes()){
            gen.writeStartObject();
            gen.writeObjectField("start", openedTime.getStartTime());
            gen.writeObjectField("end", openedTime.getEndTime());
            gen.writeEndObject();
        }
        
        gen.writeEndArray();

        gen.writeEndObject();

        gen.writeObjectFieldStart("geometry");
        
        gen.writeStringField("type", "Point");

        gen.writeArrayFieldStart("coordinates");
        gen.writeObject(restaurant.getPlace().getLatitude());
        gen.writeObject(restaurant.getPlace().getLongitude());
        gen.writeEndArray();

        gen.writeEndObject();
    }
}
