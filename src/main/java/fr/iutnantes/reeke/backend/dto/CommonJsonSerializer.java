package fr.iutnantes.reeke.backend.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import fr.iutnantes.reeke.backend.Constants;
import fr.iutnantes.reeke.backend.entity.api.ApiError;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

/**
 * Classe contenant les traitements commun de la sérialisation.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class CommonJsonSerializer {
    /**
     * Constructeur par défaut.
     */
    private CommonJsonSerializer() {
    }

    /**
     * Écrire le début du JSON.
     *
     * @param gen Le générateur de JSON.
     * @throws IOException Erreur d'entrée/sortie.
     */
    public static void startSerializing(JsonGenerator gen) throws IOException {
        gen.writeStartObject();

        gen.writeObjectFieldStart("jsonapi");
        gen.writeObjectField("version", "1.0");
        gen.writeEndObject();
    }

    /**
     * Écrire du JSON pour retourner des erreurs.
     *
     * @param gen    Le générateur de JSON.
     * @param errors Les erreurs {@link ApiError}.
     * @throws IOException Erreur d'entrée/sortie.
     */
    public static void finishSerializing(JsonGenerator gen, @NotNull List<ApiError> errors) throws IOException {
        gen.writeArrayFieldStart("errors");

        for (ApiError apiError : errors) {
            gen.writeStartObject();
            gen.writeObjectField("id", apiError.getId());

            gen.writeObjectFieldStart("links");
            gen.writeObjectField("about", apiError.getAboutLinks());
            gen.writeEndObject();

            gen.writeObjectField("status", apiError.getStatus());
            gen.writeObjectField("code", apiError.getCode());
            gen.writeObjectField("title", apiError.getTitle());
            gen.writeObjectField("detail", apiError.getDetail());

            gen.writeObjectFieldStart("source");
            gen.writeObjectField("pointer", apiError.getSource().getPointer());
            if (apiError.getSource().getPointer() != null) {
                gen.writeObjectField("parameter", apiError.getSource().getParameter());
            }
            gen.writeEndObject();

            gen.writeEndObject();
        }

        gen.writeEndArray();

        gen.writeObjectField("meta", null);

        gen.writeEndObject();
    }

    /**
     * Écrire la fin du JSON.
     *
     * @param gen Le générateur de JSON.
     * @throws IOException Erreur d'entrée/sortie.
     */
    public static void finishSerializing(JsonGenerator gen) throws IOException {
        gen.writeObjectField("errors", null);

        gen.writeObjectFieldStart("meta");
        gen.writeArrayFieldStart("sources");
        for (String source : Constants.SOURCES_LIST) {
            gen.writeString(source);
        }
        gen.writeEndArray();
        gen.writeEndObject();

        gen.writeEndObject();
    }
}
