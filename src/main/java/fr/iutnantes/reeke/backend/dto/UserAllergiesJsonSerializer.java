package fr.iutnantes.reeke.backend.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import fr.iutnantes.reeke.backend.entity.Allergy;
import fr.iutnantes.reeke.backend.entity.UserAllergies;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

/**
 * User JSON Serializer au format JSON Api.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see JsonSerializer
 */
@JsonComponent
public class UserAllergiesJsonSerializer extends JsonSerializer<UserAllergies> {
    @Override
    public void serialize(UserAllergies value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        CommonJsonSerializer.startSerializing(gen);

        gen.writeObjectFieldStart("data");

        gen.writeStringField("id", value.getLogin());
        gen.writeStringField("type", "allergies");

        gen.writeObjectFieldStart("attributes");
        gen.writeArrayFieldStart("allergies");
        for (Allergy allergy : value.getAllergies()) {
            gen.writeString(allergy.getName());
        }
        gen.writeEndArray();
        gen.writeEndObject();

        gen.writeEndObject();

        CommonJsonSerializer.finishSerializing(gen);
    }
}
