package fr.iutnantes.reeke.backend.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import fr.iutnantes.reeke.backend.entity.Allergy;
import fr.iutnantes.reeke.backend.entity.Dish;
import fr.iutnantes.reeke.backend.entity.Image;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

/**
 * Dish JSON Serializer au format JSON Api.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see JsonSerializer
 */
@JsonComponent
public class DishJsonSerializer extends JsonSerializer<Dish> {
    /**
     * Sérialiser le {@link Dish}.
     *
     * @param dish Le plat.
     * @param gen  Le générateur de JSON.
     * @throws IOException Exception.
     */
    public static void serializeDish(Dish dish, JsonGenerator gen) throws IOException {
        gen.writeStringField("id", dish.getId().toString());
        gen.writeStringField("type", "dish");

        gen.writeObjectFieldStart("attributes");
        gen.writeObjectField("name", dish.getName());
        gen.writeObjectField("description", dish.getDescription());
        gen.writeObjectField("price", dish.getPrice());
        gen.writeObjectField("category", dish.getCategory().getName());

        gen.writeObjectField("note", dish.getNote());

        gen.writeArrayFieldStart("allergies");
        for (Allergy allergy : dish.getAllergies()) {
            gen.writeString(allergy.getName());
        }
        gen.writeEndArray();

        gen.writeArrayFieldStart("images");
        for (Image image : dish.getImages()) {
            gen.writeStartObject();
            gen.writeObjectField("url", image.getUrl());
            gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeEndObject();
    }

    @Override
    public void serialize(Dish value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        CommonJsonSerializer.startSerializing(gen);

        gen.writeObjectFieldStart("data");

        DishJsonSerializer.serializeDish(value, gen);

        gen.writeEndObject();

        CommonJsonSerializer.finishSerializing(gen);
    }
}
