package fr.iutnantes.reeke.backend.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import fr.iutnantes.reeke.backend.entity.Token;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

/**
 * Serializer pour les {@link Token}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see com.fasterxml.jackson.databind.JsonSerializer
 */
@JsonComponent
public class TokenJsonSerializer extends JsonSerializer<Token> {
    @Override
    public void serialize(Token value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        CommonJsonSerializer.startSerializing(gen);

        gen.writeObjectFieldStart("data");

        gen.writeStringField("id", value.getId().toString());
        gen.writeStringField("type", "token");

        gen.writeObjectFieldStart("attributes");
        gen.writeObjectField("access_token", value.getAccessToken());
        gen.writeObjectField("expires_in", value.getExpiresIn());
        gen.writeEndObject();

        gen.writeEndObject();

        CommonJsonSerializer.finishSerializing(gen);
    }
}
