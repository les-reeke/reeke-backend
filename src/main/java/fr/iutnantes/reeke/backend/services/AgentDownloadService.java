package fr.iutnantes.reeke.backend.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.iutnantes.reeke.backend.Constants;
import fr.iutnantes.reeke.backend.entity.Category;
import fr.iutnantes.reeke.backend.entity.Location;
import fr.iutnantes.reeke.backend.entity.OpenedTime;
import fr.iutnantes.reeke.backend.entity.Restaurant;
import fr.iutnantes.reeke.backend.repository.CategoriesRepository;
import fr.iutnantes.reeke.backend.repository.LocationsRepository;
import fr.iutnantes.reeke.backend.repository.OpenedTimesRepository;
import fr.iutnantes.reeke.backend.repository.RestaurantsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.SecureRandom;
import java.sql.Time;
import java.util.Calendar;

/**
 * Service de récupération des données auprès d'Agent.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
@Component
@Profile("!test")
public class AgentDownloadService implements ApplicationListener<ApplicationReadyEvent> {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AgentDownloadService.class);

    /**
     * Liste des noms des départements.
     */
    private static final String[] DEPARTMENT_NAMES = new String[]{"Ain - Bourg-en-bresse", "Aisne - Laon", "Allier - Moulins", "Alpes-de-Haute-Provence - Digne-les-bains",
            "Hautes-alpes - Gap", "Alpes-maritimes - Nice", "Ardèche - Privas", "Ardennes - Charleville-mézières", "Ariège - Foix",
            "Aube - Troyes", "Aude - Carcassonne", "Aveyron - Rodez", "Bouches-du-Rhône - Marseille", "Calvados - Caen", "Cantal - Aurillac",
            "Charente - Angoulême", "Charente-maritime - La rochelle", "Cher - Bourges", "Corrèze - Tulle", "Corse", "Côte-d'Or - Dijon",
            "Côtes-d'Armor - Saint-brieuc", "Creuse - Guéret", "Dordogne - Périgueux", "Doubs - Besançon", "Drôme - Valence", "Eure - Évreux",
            "Eure-et-loir - Chartres", "Finistère - Quimper", "Gard - Nîmes", "Haute-garonne - Toulouse", "Gers - Auch", "Gironde - Bordeaux",
            "Hérault - Montpellier", "Ille-et-vilaine - Rennes", "Indre - Châteauroux", "Indre-et-loire - Tours", "Isère - Grenoble",
            "Jura - Lons-le-saunier", "Landes - Mont-de-marsan", "Loir-et-cher - Blois", "Loire - Saint-étienne", "Haute-loire - Le puy-en-velay",
            "Loire-atlantique - Nantes", "Loiret - Orléans", "Lot - Cahors", "Lot-et-garonne - Agen", "Lozère - Mende", "Maine-et-loire - Angers",
            "Manche - Saint-lô", "Marne - Châlons-en-champagne", "Haute-marne - Chaumont", "Mayenne - Laval", "Meurthe-et-moselle - Nancy",
            "Meuse - Bar-le-duc", "Morbihan - Vannes", "Moselle - Metz", "Nièvre - Nevers", "Nord - Lille", "Oise - Beauvais", "Orne - Alençon",
            "Pas-de-calais - Arras", "Puy-de-dôme - Clermont-ferrand", "Pyrénées-atlantiques - Pau", "Hautes-Pyrénées - Tarbes",
            "Pyrénées-orientales - Perpignan", "Bas-rhin - Strasbourg", "Haut-rhin - Colmar", "Rhône - Lyon", "Haute-saône - Vesoul",
            "Saône-et-loire - Mâcon", "Sarthe - Le mans", "Savoie - Chambéry", "Haute-savoie - Annecy", "Paris - Paris",
            "Seine-maritime - Rouen", "Seine-et-marne - Melun", "Yvelines - Versailles", "Deux-sèvres - Niort", "Somme - Amiens",
            "Tarn - Albi", "Tarn-et-Garonne - Montauban", "Var - Toulon", "Vaucluse - Avignon", "Vendée - La roche-sur-yon",
            "Vienne - Poitiers", "Haute-vienne - Limoges", "Vosges - Épinal", "Yonne - Auxerre", "Territoire de belfort - Belfort",
            "Essonne - Évry", "Hauts-de-seine - Nanterre", "Seine-Saint-Denis - Bobigny", "Val-de-marne - Créteil",
            "Val-d'Oise - Cergy Pontoise"};

    /**
     * Repository.
     */
    private final LocationsRepository locationsRepository;
    private final RestaurantsRepository restaurantsRepository;
    private final OpenedTimesRepository openedTimesRepository;
    private final CategoriesRepository categoriesRepository;

    /**
     * Constructeur par défaut.
     *
     * @param locationsRepository   Repository des emplacements (injecté par Spring).
     * @param restaurantsRepository Repository des restaurants (injecté par Spring).
     * @param openedTimesRepository Repository des horaires d'ouverture (injecté par Spring).
     * @param categoriesRepository  Repository des catégories (injecté par Spring).
     */
    @Autowired
    public AgentDownloadService(LocationsRepository locationsRepository, RestaurantsRepository restaurantsRepository, OpenedTimesRepository openedTimesRepository, CategoriesRepository categoriesRepository) {
        this.locationsRepository = locationsRepository;
        this.restaurantsRepository = restaurantsRepository;
        this.openedTimesRepository = openedTimesRepository;
        this.categoriesRepository = categoriesRepository;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(Constants.URL_AGENT_JSON, String.class);
        LOGGER.info("Read data from Agent source");

        if (response.getStatusCode().equals(HttpStatus.OK)) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                JsonNode root = mapper.readTree(response.getBody());

                if (root.isArray()) {
                    int progress = 0;
                    LOGGER.info("Load restaurants from Agent source {} / {}", progress, root.size());

                    for (JsonNode restaurantRootNode : root) {
                        loadRestaurantDataIfNeeded(restaurantRootNode);

                        progress++;
                        LOGGER.info("Load restaurants from Agent source {} / {}", progress, root.size());
                    }
                }
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    /**
     * Chargé les données du restaurant en base si elles n'existent.
     *
     * @param restaurantRootNode Le JSON du restaurant.
     */
    private void loadRestaurantDataIfNeeded(JsonNode restaurantRootNode) {
        JsonNode fields = restaurantRootNode.get("fields");

        StringBuilder adress = new StringBuilder();
        if (fields.has("adresse_1")) {
            adress.append(fields.get("adresse_1").asText());
        }
        if (fields.has("adresse_2")) {
            if (adress.length() > 0) {
                adress.append(" - ");
            }
            adress.append(fields.get("adresse_2").asText());
        }
        if (fields.has("adresse_3")) {
            if (adress.length() > 0) {
                adress.append(" - ");
            }
            adress.append(fields.get("adresse_3").asText());
        }

        Location location = locationsRepository.findOneByAdressAndPostalCodeAndCountry(adress.toString(), fields.get("code_posta").asInt(), "France");

        if (location == null) {
            location = new Location(adress.toString(), fields.get("code_posta").asInt(),
                    fields.get("commune").asText(), DEPARTMENT_NAMES[fields.get("code_posta").asInt() / 1000],
                    "France", fields.get("latitude").asDouble(), fields.get("longitude").asDouble());
            locationsRepository.save(location);
        }

        if (restaurantsRepository.findByNameAndPlace(fields.get("raison_soc").asText(), location, PageRequest.of(0, 1)).getNumberOfElements() <= 0) {
            Category category = categoriesRepository.findOneByName("Restaurants");
            if (category == null) {
                category = new Category("Restaurants");
                categoriesRepository.save(category);
            }

            Restaurant restaurant = new Restaurant(fields.get("raison_soc").asText(), fields.has("telephon") ? fields.get("telephon").asText() : null,
                    fields.has("mail") ? fields.get("mail").asText() : null, fields.has("site_inter") ? fields.get("site_inter").asText() : null,
                    location, category, 0.00);
            restaurantsRepository.save(restaurant);

            // FIXME : Mock des horaires d'ouvertures.
            Calendar calendar1 = Calendar.getInstance();
            Calendar calendar2 = Calendar.getInstance();
            Calendar calendar3 = Calendar.getInstance();
            Calendar calendar4 = Calendar.getInstance();

            for (int i = 0; i < 7; i++) {
                calendar1.set(Calendar.HOUR_OF_DAY, new SecureRandom().nextInt() % 2 + 7);
                calendar1.set(Calendar.MINUTE, new SecureRandom().nextInt() % 3 * 15);
                calendar2.set(Calendar.HOUR_OF_DAY, new SecureRandom().nextInt() % 3 + 10);
                calendar2.set(Calendar.MINUTE, new SecureRandom().nextInt() % 3 * 15);
                calendar3.set(Calendar.HOUR_OF_DAY, new SecureRandom().nextInt() % 3 + 14);
                calendar3.set(Calendar.MINUTE, new SecureRandom().nextInt() % 3 * 15);
                calendar4.set(Calendar.HOUR_OF_DAY, new SecureRandom().nextInt() % 4 + 18);
                calendar4.set(Calendar.MINUTE, new SecureRandom().nextInt() % 3 * 15);

                OpenedTime openedTime = new OpenedTime(i, new Time(calendar1.getTimeInMillis()), new Time(calendar2.getTimeInMillis()));
                openedTimesRepository.save(openedTime);
                OpenedTime openedTime2 = new OpenedTime(i, new Time(calendar3.getTimeInMillis()), new Time(calendar4.getTimeInMillis()));
                openedTimesRepository.save(openedTime2);

                restaurant.addOpenedTime(openedTime);
                restaurant.addOpenedTime(openedTime2);
                restaurantsRepository.save(restaurant);
            }
        }
    }
}
