package fr.iutnantes.reeke.backend.exception;

/**
 * Exception indiquant que le plat est invalide (inexistant).
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class InvalidDishException extends RuntimeException {
    /**
     * Constructeur par défaut.
     */
    public InvalidDishException() {
        super("Dish is invalid");
    }

    /**
     * Constructeur avec un message spécifique.
     *
     * @param message Le message.
     */
    public InvalidDishException(String message) {
        super(message);
    }
}
