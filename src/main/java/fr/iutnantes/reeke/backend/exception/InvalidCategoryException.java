package fr.iutnantes.reeke.backend.exception;

/**
 * Exception indiquant que la catégorie est invalide (inexistant).
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class InvalidCategoryException extends RuntimeException {
    /**
     * Constructeur par défaut.
     */
    public InvalidCategoryException() {
        super("Category is invalid");
    }

    /**
     * Constructeur avec un message spécifique.
     *
     * @param message Le message.
     */
    public InvalidCategoryException(String message) {
        super(message);
    }
}
