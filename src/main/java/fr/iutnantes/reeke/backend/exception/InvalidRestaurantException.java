package fr.iutnantes.reeke.backend.exception;

/**
 * Exception indiquant que le restaurant est invalide (inexistant).
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class InvalidRestaurantException extends RuntimeException {
    /**
     * Constructeur par défaut.
     */
    public InvalidRestaurantException() {
        super("Restaurant is invalid");
    }

    /**
     * Constructeur avec un message spécifique.
     *
     * @param message Le message.
     */
    public InvalidRestaurantException(String message) {
        super(message);
    }
}
