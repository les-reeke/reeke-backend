package fr.iutnantes.reeke.backend.exception;

/**
 * Exception indiquant que les informations d'authentification sont invalides.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class InvalidCredentialsException extends RuntimeException {
    /**
     * Constructeur par défaut.
     */
    public InvalidCredentialsException() {
        super("Credentials are invalid");
    }

    /**
     * Constructeur avec un message spécifique.
     *
     * @param message Le message.
     */
    public InvalidCredentialsException(String message) {
        super(message);
    }
}
