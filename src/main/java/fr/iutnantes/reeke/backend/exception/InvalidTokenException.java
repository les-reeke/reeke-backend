package fr.iutnantes.reeke.backend.exception;

/**
 * Exception indiquant que le token sont invalides.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class InvalidTokenException extends RuntimeException {
    /**
     * Constructeur par défaut.
     */
    public InvalidTokenException() {
        super("Token is invalid");
    }

    /**
     * Constructeur avec un message spécifique.
     *
     * @param message Le message.
     */
    public InvalidTokenException(String message) {
        super(message);
    }
}
