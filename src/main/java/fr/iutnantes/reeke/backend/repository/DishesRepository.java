package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.Dish;
import fr.iutnantes.reeke.backend.entity.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * Repository pour les {@link Dish}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see org.springframework.data.repository.PagingAndSortingRepository
 */
public interface DishesRepository extends PagingAndSortingRepository<Dish, Integer> {
    /**
     * Lister tous les plats.
     *
     * @return Les plats.
     */
    List<Dish> findAll();

    /**
     * Récupérer les plats d'un restaurant.
     *
     * @param restaurant Le restaurant.
     * @param pageable   {@link Pageable}.
     * @return Une page contenant les plats.
     */
    Page<Dish> findByRestaurants(Restaurant restaurant, Pageable pageable);

    /**
     * Rechercher des plats sans allergies.
     *
     * @param restaurantId   L'id du restaurant.
     * @param allergiesNames Les nom des allergies.
     * @param pageable       {@link Pageable}.
     * @return Une page contenant les plats.
     */
    @Query(value = "SELECT *\n" +
            "FROM dishes dis\n" +
            "INNER JOIN (SELECT *\n" +
            "  FROM restaurants_dishes rd\n" +
            "  WHERE rd.restaurant_id = :restaurantId) restDis ON restDis.dish_id = dis.id\n" +
            "INNER JOIN restaurants rest ON rest.id = restDis.restaurant_id\n" +
            " LEFT JOIN allergies_dishes allDis ON allDis.dish_id = dis.id" +
            " LEFT JOIN allergies aller ON aller.name = allDis.allergy_id" +
            " WHERE NOT EXISTS (SELECT *\n" +
            "  FROM allergies_dishes allDis2\n" +
            "  WHERE allDis2.dish_id = dis.id\n" +
            "  AND allDis2.allergy_id IN :allergiesNames)",
            countQuery = "SELECT count(*)\n" +
                    "FROM dishes dis\n" +
                    "INNER JOIN (SELECT *\n" +
                    "  FROM restaurants_dishes rd\n" +
                    "  WHERE rd.restaurant_id = :restaurantId) restDis ON restDis.dish_id = dis.id\n" +
                    " INNER JOIN restaurants rest ON rest.id = restDis.restaurant_id\n" +
                    " LEFT JOIN allergies_dishes allDis ON allDis.dish_id = dis.id" +
                    " LEFT JOIN allergies aller ON aller.name = allDis.allergy_id" +
                    " WHERE NOT EXISTS (SELECT *\n" +
                    "  FROM allergies_dishes allDis2\n" +
                    "  WHERE allDis2.dish_id = dis.id\n" +
                    "  AND allDis2.allergy_id IN :allergiesNames)",
            nativeQuery = true)
    Page<Dish> findAllByRestaurantWithoutAllergies(@Param("restaurantId") Integer restaurantId, @Param("allergiesNames") Collection<String> allergiesNames, Pageable pageable);


    /**
     * Rechercher des plats sans allergies contenant un mot dans leur nom.
     *
     * @param restaurantId   L'id du restaurant.
     * @param allergiesNames Les nom des allergies.
     * @param word           Le mot à rechercher dans le nom.
     * @param pageable       {@link Pageable}.
     * @return Une page contenant les plats.
     */
    @Query(value = "SELECT *\n" +
            "FROM dishes dis\n" +
            "INNER JOIN (SELECT *\n" +
            "  FROM restaurants_dishes rd\n" +
            "  WHERE rd.restaurant_id = :restaurantId) restDis ON restDis.dish_id = dis.id\n" +
            " INNER JOIN restaurants rest ON rest.id = restDis.restaurant_id\n" +
            " LEFT JOIN allergies_dishes allDis ON allDis.dish_id = dis.id" +
            " LEFT JOIN allergies aller ON aller.name = allDis.allergy_id" +
            " WHERE NOT EXISTS (SELECT *\n" +
            "  FROM allergies_dishes allDis2\n" +
            "  WHERE allDis2.dish_id = dis.id\n" +
            "  AND allDis2.allergy_id IN :allergiesNames)" +
            " AND LOWER(dis.name) LIKE %:word%",
            countQuery = "SELECT count(*)\n" +
                    "FROM dishes dis\n" +
                    "INNER JOIN (SELECT *\n" +
                    "  FROM restaurants_dishes rd\n" +
                    "  WHERE rd.restaurant_id = :restaurantId) restDis ON restDis.dish_id = dis.id\n" +
                    " INNER JOIN restaurants rest ON rest.id = restDis.restaurant_id\n" +
                    " LEFT JOIN allergies_dishes allDis ON allDis.dish_id = dis.id" +
                    " LEFT JOIN allergies aller ON aller.name = allDis.allergy_id" +
                    " WHERE NOT EXISTS (SELECT *\n" +
                    "  FROM allergies_dishes allDis2\n" +
                    "  WHERE allDis2.dish_id = dis.id\n" +
                    "  AND allDis2.allergy_id IN :allergiesNames)" +
                    " AND LOWER(dis.name) LIKE %:word%",
            nativeQuery = true)
    Page<Dish> findAllByRestaurantWithoutAllergiesAndNameContainingIgnoreCase(@Param("restaurantId") Integer restaurantId, @Param("allergiesNames") List<String> allergiesNames, @Param("word") String word, Pageable pageable);

    /**
     * Rechercher des plats dont leur nom contient un mot.
     *
     * @param restaurant Le restaurant.
     * @param name       Le mot.
     * @param pageable   {@link Pageable}.
     * @return Une page contenant les plats.
     */
    Page<Dish> findAllByRestaurantsAndNameContainingIgnoreCase(Restaurant restaurant, String name, Pageable pageable);
}
