package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.Image;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/**
 * Repository pour les {@link Image}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see org.springframework.data.repository.PagingAndSortingRepository
 */
public interface ImagesRepository extends PagingAndSortingRepository<Image, Integer> {
    /**
     * Rechercher une image avec l'URL.
     *
     * @param imageURL L'URL.
     * @return L'image si elle existe.
     */
    Optional<Image> findOneByUrl(String imageURL);
}
