package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.Dish;
import fr.iutnantes.reeke.backend.entity.User;
import fr.iutnantes.reeke.backend.entity.note.Note;
import fr.iutnantes.reeke.backend.entity.note.NoteId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Repository pour les {@link Note}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see PagingAndSortingRepository
 */
public interface NotesRepository extends PagingAndSortingRepository<Note, NoteId> {
    /**
     * Rechercher une note pour un plat par un utilisateur.
     *
     * @param user L'utilisateur.
     * @param dish Le plat.
     * @return La note de l'utilisateur sur le plat.
     */
    Optional<Note> findOneByUserAndDish(User user, Dish dish);

    /**
     * Supprimer les allergies rattachées à l'utilisateur.
     *
     * @param user L'utilisateur.
     * @return Le nombre de lignes supprimées.
     */
    @Modifying
    @Transactional
    @Query("DELETE FROM Note WHERE user = :user")
    int deleteByUser(@Param("user") User user);
}
