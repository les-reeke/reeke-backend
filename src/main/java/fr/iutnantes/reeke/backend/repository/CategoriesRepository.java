package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.Category;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/**
 * Repository pour les {@link Category}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see org.springframework.data.repository.PagingAndSortingRepository
 */
public interface CategoriesRepository extends PagingAndSortingRepository<Category, String> {
    Category findOneByName(String name);

    /**
     * Rechercher une catégorie sans prendre en compte la casse.
     *
     * @param name Le mot
     * @return Le mot.
     */
    Optional<Category> findOneByNameIgnoreCase(String name);
}
