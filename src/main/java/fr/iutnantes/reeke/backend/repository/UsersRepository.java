package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository pour les {@link User}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see org.springframework.data.repository.PagingAndSortingRepository
 */
public interface UsersRepository extends PagingAndSortingRepository<User, String> {
}
