package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.Dish;
import fr.iutnantes.reeke.backend.entity.Location;
import fr.iutnantes.reeke.backend.entity.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Time;
import java.util.List;
import java.util.Optional;

/**
 * Repository pour les {@link Restaurant}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see org.springframework.data.repository.PagingAndSortingRepository
 */
public interface RestaurantsRepository extends PagingAndSortingRepository<Restaurant, Integer> {
    Page<Restaurant> findByNameAndPlace(String name, Location place, Pageable pageable);

    /**
     * liste des restaurant
     *
     * @return Les plats.
     */
    Page<Restaurant> findAll();

    /**
     * page de restaurant par id
     *
     * @param id       id du restaurant
     * @param pageable pageable
     * @return des restaurants
     */
    Page<Restaurant> findOneById(Integer id, Pageable pageable);

    /**
     * Rechercher le restaurant associé au plat.
     *
     * @param dish Le plat.
     * @return Le restaurant si il existe.
     */
    Optional<Restaurant> findOneByDishes(Dish dish);

    /**
     * page de restaurant par nom
     *
     * @param name     nom du restaurant
     * @param pageable pageable
     * @return des restaurants
     */
    Page<Restaurant> findOneByNameContainingIgnoreCase(String name, Pageable pageable);

    Page<Restaurant> findAllByNote(Double note, Pageable pageable);

    @Query(value = "select r.* from restaurants r, openedTime ot where r.id = ot.restaurant_id and ot.starttime > :begin", nativeQuery = true)
    Page<Restaurant> findByBegin(@Param("begin") Time begin, Pageable pageable);

    @Query(value = "select r.* from restaurants r, openedTime ot where r.id = ot.restaurant_id and ot.endtime > :end", nativeQuery = true)
    Page<Restaurant> findByEnd(@Param("end") Time end, Pageable pageable);

    Page<Restaurant> findAllByOrderByIdAsc(Pageable pageable);
    Page<Restaurant> findAllByOrderByNameAsc(Pageable pageable);
    Page<Restaurant> findAllByOrderByNoteAsc(Pageable pageable);

    Page<Restaurant> findAllByIdOrderByIdAsc(Integer id, Pageable pageable);
    Page<Restaurant> findAllByNameContainingIgnoreCaseOrderByNameAsc(String name, Pageable pageable);
    Page<Restaurant> findAllByNoteOrderByNoteAsc(Double note, Pageable pageable);

}
