package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.OpenedTime;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository pour les {@link OpenedTime}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see PagingAndSortingRepository
 */
public interface OpenedTimesRepository extends PagingAndSortingRepository<OpenedTime, Integer> {
}
