package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.Allergy;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/**
 * Repository pour les {@link Allergy}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see org.springframework.data.repository.PagingAndSortingRepository
 */
public interface AllergiesRepository extends PagingAndSortingRepository<Allergy, String> {
     Optional<Allergy> findOneByNameIgnoreCase(String name);
}
