package fr.iutnantes.reeke.backend.repository;

import fr.iutnantes.reeke.backend.entity.Location;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository pour les {@link Location}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see org.springframework.data.repository.PagingAndSortingRepository
 */
public interface LocationsRepository extends PagingAndSortingRepository<Location, Integer> {
    Location findOneByAdressAndPostalCodeAndCountry(String adress, Integer postalCode, String country);
}
