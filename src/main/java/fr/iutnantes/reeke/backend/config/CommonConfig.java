package fr.iutnantes.reeke.backend.config;

import fr.iutnantes.reeke.backend.util.TokenEngine;
import org.jose4j.lang.JoseException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

/**
 * Configuration commune à tous les profiles.
 *
 * @author Thibaut PICHON.
 * @version 0.1
 */
@Configuration
public class CommonConfig {
    /**
     * Généner le bean contenant le gestionnaire de tokens.
     *
     * @return Le gestionnaire de tokens.
     * @throws JoseException Erreur lors de la création de la clé RSA pour les JsonWebToken.
     */
    @Bean
    @Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public TokenEngine getTokenValidator() throws JoseException {
        return new TokenEngine();
    }
}
