package fr.iutnantes.reeke.backend.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.iutnantes.reeke.backend.entity.Token;
import fr.iutnantes.reeke.backend.entity.User;
import fr.iutnantes.reeke.backend.repository.UsersRepository;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Calendar;
import java.util.Optional;

/**
 * Gestionnaire des tokens.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class TokenEngine {
    /**
     * Clé RSA pour les JsonWebToken.
     */
    private final RsaJsonWebKey rsaJsonWebKey;

    /**
     * Repository des utilisateurs.
     */
    @Autowired
    UsersRepository usersRepository;

    /**
     * Constructeur.
     *
     * @throws JoseException Erreur lors de la création de la clé RSA pour les tokens.
     */
    public TokenEngine() throws JoseException {
        this.rsaJsonWebKey = RsaJwkGenerator.generateJwk(4096);
        rsaJsonWebKey.setKeyId("reeke-1");
    }

    /**
     * Générer un token avec les informations de l'utilisateur.
     *
     * @param user L'utilisateur.
     * @return Le token.
     * @throws JoseException           Erreur survenu lors de la génération du JsonWebToken.
     * @throws MalformedClaimException Erreur lors de l'enregistrement des données dans le JsonWebToken.
     * @throws JsonProcessingException Exception.
     */
    public Token generateToken(User user) throws JoseException, MalformedClaimException, JsonProcessingException {
        JwtClaims jwtClaims = new JwtClaims();
        jwtClaims.setIssuer("api.partagetesco.fr");
        jwtClaims.setAudience("Client");
        jwtClaims.setExpirationTimeMinutesInTheFuture(1440f);
        jwtClaims.setGeneratedJwtId();
        jwtClaims.setIssuedAtToNow();
        jwtClaims.setNotBeforeMinutesInThePast(0f);
        jwtClaims.setSubject("Authentication Token");
        jwtClaims.setStringClaim("user", new ObjectMapper().writeValueAsString(user));

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(jwtClaims.toJson());
        jws.setKey(rsaJsonWebKey.getPrivateKey());
        jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_PSS_USING_SHA512);

        return new Token(
                Calendar.getInstance().getTime(),
                jws.getCompactSerialization(),
                jwtClaims.getExpirationTime().getValueInMillis());
    }

    /**
     * Vérifier si un token est valide (valide & utilisateur existe).
     *
     * @param token Le token.
     * @return Vrai si valide, faux sinon.
     */
    public boolean isValidToken(String token) {
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime()
                .setAllowedClockSkewInSeconds(30)
                .setRequireSubject()
                .setExpectedIssuer("api.partagetesco.fr")
                .setExpectedAudience("Client")
                .setVerificationKey(rsaJsonWebKey.getKey())
                .setJwsAlgorithmConstraints(
                        AlgorithmConstraints.ConstraintType.WHITELIST,
                        AlgorithmIdentifiers.RSA_PSS_USING_SHA512)
                .build();

        try {
            JwtClaims jwtClaimsRes = jwtConsumer.processToClaims(token);
            User user = new ObjectMapper().readValue(jwtClaimsRes.getClaimValueAsString("user"), User.class);
            return usersRepository.existsById(user.getLogin());
        } catch (IOException | InvalidJwtException ignored) {
        }

        return false;
    }

    /**
     * Récupére l'utilisateur associé au token.
     *
     * @param token Le token.
     * @return L'utilisateur associé, null si le token est invalide.
     */
    public Optional<User> readToken(String token) {
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime()
                .setAllowedClockSkewInSeconds(30)
                .setRequireSubject()
                .setExpectedIssuer("api.partagetesco.fr")
                .setExpectedAudience("Client")
                .setVerificationKey(rsaJsonWebKey.getKey())
                .setJwsAlgorithmConstraints(
                        AlgorithmConstraints.ConstraintType.WHITELIST,
                        AlgorithmIdentifiers.RSA_PSS_USING_SHA512)
                .build();

        try {
            JwtClaims jwtClaimsRes = jwtConsumer.processToClaims(token);
            User user = new ObjectMapper().readValue(jwtClaimsRes.getClaimValueAsString("user"), User.class);
            return usersRepository.findById(user.getLogin());
        } catch (IOException | InvalidJwtException ignored) {
        }

        return Optional.empty();
    }
}
