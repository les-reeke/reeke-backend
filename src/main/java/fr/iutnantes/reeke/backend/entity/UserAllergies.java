package fr.iutnantes.reeke.backend.entity;

import java.util.Set;

/**
 * Classe de surcouche à un utilisateur pour différencier la sérialisation.
 * <p>
 * FIXME : Voir si on peut changer le Serializer utilisé par Jackson juste pour pour certain web services.
 *
 * @author Thibaut PICHON
 * @version 0.1
 * @see fr.iutnantes.reeke.backend.entity.User
 */
public class UserAllergies {
    /**
     * L'utilisateur.
     */
    private final User user;

    /**
     * Constructeur par défaut.
     *
     * @param user L'utilisateur.
     */
    public UserAllergies(User user) {
        this.user = user;
    }

    /**
     * Getter pour l'utilisateur.
     *
     * @return L'utlisateur.
     */
    public User getUser() {
        return user;
    }

    /**
     * Getter pour le login.
     *
     * @return Le login.
     */
    public String getLogin() {
        return this.user.getLogin();
    }

    /**
     * Getter pour les allergies.
     *
     * @return Les allergies.
     */
    public Set<Allergy> getAllergies() {
        return this.user.getAllergies();
    }
}
