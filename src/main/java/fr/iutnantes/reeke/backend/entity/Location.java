package fr.iutnantes.reeke.backend.entity;

import javax.persistence.*;
import java.util.Objects;

/**
 * Entité représentant un emplacement.
 *
 * @author Jocelyn ROUL
 * @version 0.1
 * @see Entity
 */
@Entity
@Table(name = "locations")
public class Location {

    /**
     * Identifiant du lieux.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * Adresse du lieux.
     */
    @Column(name = "adress")
    private String adress;

    /**
     * Code postal du lieux.
     */
    @Column(name = "postalcode")
    private Integer postalCode;

    /**
     * Code municipal du lieux.
     */
    @Column(name = "municipalityinseecode")
    private Integer municipalityInseeCode;

    /**
     * Municipal du lieux.
     */
    @Column(name = "municipality")
    private String municipality;

    /**
     * Département du lieux.
     */
    @Column(name = "department")
    private String department;

    /**
     * Pays du lieux.
     */
    @Column(name = "country")
    private String country;

    /**
     * Latitude du lieux.
     */
    @Column(name = "latitude")
    private Double latitude;

    /**
     * Longitude du lieux.
     */
    @Column(name = "longitude")
    private Double longitude;

    /**
     * Constructeur par défaut pour la sérialisation/désérialisation.
     */
    public Location() {
    }

    /**
     * Constructeur.
     *
     * @param adress       L'adresse.
     * @param postalCode   Le code postal.
     * @param municipality La municipalité.
     * @param department   Le département.
     * @param country      Le pays.
     * @param latitude     La latitude.
     * @param longitude    La longitude.
     */
    public Location(String adress, Integer postalCode, String municipality,
                    String department, String country, Double latitude, Double longitude) {
        this.adress = adress;
        this.postalCode = postalCode;
        this.municipality = municipality;
        this.department = department;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Getter pour l'adresse.
     *
     * @return L'adresse.
     */
    public String getAdress() {
        return adress;
    }

    /**
     * Setter pour l'adresse.
     *
     * @param adress L'adresse.
     */
    public void setAdress(String adress) {
        this.adress = adress;
    }

    /**
     * Getter pour le code postal.
     *
     * @return Le code postal.
     */
    public Integer getPostalCode() {
        return postalCode;
    }

    /**
     * Setter pour le code postal.
     *
     * @param postalCode Le code postal.
     */
    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Getter pour le code INSEE de la municipalité.
     *
     * @return Le code INSEE de la municipalité.
     */
    public Integer getMunicipalityInseeCode() {
        return municipalityInseeCode;
    }

    /**
     * Setter pour le code INSEE de la municipalité.
     *
     * @param municipalityInseeCode Le code INSEE de la municipalité.
     */
    public void setMunicipalityInseeCode(Integer municipalityInseeCode) {
        this.municipalityInseeCode = municipalityInseeCode;
    }

    /**
     * Getter pour la municipalité.
     *
     * @return La municipalité.
     */
    public String getMunicipality() {
        return municipality;
    }

    /**
     * Setter pour la municipalité.
     *
     * @param municipality La municipalité.
     */
    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    /**
     * Getter pour le département.
     *
     * @return Le département.
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Setter pour le département.
     *
     * @param department Le département.
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * Getter pour le pays.
     *
     * @return Le pays.
     */
    public String getCountry() {
        return country;
    }

    /**
     * Setter pour le pays.
     *
     * @param country Le pays.
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Getter pour la latitude.
     *
     * @return La latitude.
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Setter pour la latitude.
     *
     * @param latitude La latitude.
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Getter pour la longitude.
     *
     * @return La longitude.
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Setter pour la longitude.
     *
     * @param longitude La longitude.
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;
        Location location = (Location) o;
        return Objects.equals(id, location.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", adress='" + adress + '\'' +
                ", postalCode=" + postalCode +
                ", municipalityInseeCode=" + municipalityInseeCode +
                ", municipality='" + municipality + '\'' +
                ", department='" + department + '\'' +
                ", country='" + country + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}