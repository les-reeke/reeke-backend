package fr.iutnantes.reeke.backend.entity.note;

import fr.iutnantes.reeke.backend.entity.Dish;
import fr.iutnantes.reeke.backend.entity.Restaurant;
import fr.iutnantes.reeke.backend.entity.User;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entité représentant une note.
 *
 * @author JRoul
 * @version 0.1
 */
@Entity
@Table(name = "notes")
@IdClass(NoteId.class)
public class Note implements Serializable {
    @Id
    @Column(name = "user_id")
    private String userId;
    @Id
    @Column(name = "restaurant_id")
    private Integer restaurantId;
    @Id
    @Column(name = "dish_id")
    private Integer dishId;

    /**
     * identifiant reference user de la note
     */
    @Id
    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id", referencedColumnName = "login", insertable = false, updatable = false)
    private User user;

    /**
     * identifiant reference restaurant de la note
     */
    @Id
    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "restaurant_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Restaurant restaurant;

    /**
     * identifiant reference dish de la note
     */
    @Id
    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "dish_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Dish dish;

    /**
     * La note
     */
    @Column(name = "note")
    private Double note;

    /**
     * Constructeur vide de la note
     */
    public Note() {
    }

    public Note(User user, Restaurant restaurant, Dish dish, Double note) {
        this.user = user;
        this.userId = user.getLogin();
        this.restaurant = restaurant;
        this.restaurantId = restaurant.getId();
        this.dish = dish;
        this.dishId = dish.getId();
        this.note = note;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Double getNote() {
        return note;
    }

    public void setNote(Double note) {
        this.note = note;
    }
}