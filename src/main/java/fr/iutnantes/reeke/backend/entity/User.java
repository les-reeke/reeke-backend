package fr.iutnantes.reeke.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entité représentant un utilisateur.
 *
 * @author Jocelyn ROUL.
 * @version 0.1
 * @see Entity
 */
@Entity
@Table(name = "users")
public class User {

    /**
     * Login de l'utilisateur.
     */
    @Id
    @Column(name = "login")
    private String login;

    /**
     * Mot de passe de l'utilisateur.
     */
    @Column(name = "password")
    @JsonIgnore
    private String password;

    /**
     * Nom de l'utilisateur.
     */
    @Column(name = "firstname")
    private String firstname;

    /**
     * Prénom de l'utilisateur.
     */
    @Column(name = "lastname")
    private String lastname;

    /**
     * Email de l'utilisateur.
     */
    @Column(name = "email")
    private String email;

    /**
     * Maison de l'utilisateur.
     */
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "home_id", referencedColumnName = "id")
    private Location home;

    /**
     * Travail de l'utilisateur.
     */
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "work_id", referencedColumnName = "id")
    private Location work;

    /**
     * Ses allergies.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "allergies_users", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "allergy_id"))
    private Set<Allergy> allergies;

    /**
     * Constructeur par défaut pour la sérialisation/désérialisation.
     */
    public User() {
    }

    /**
     * Constructeur.
     *
     * @param login     Le login.
     * @param password  Le mot de passe.
     * @param firstname Le prénom.
     * @param lastname  Le nom.
     * @param email     L'adresse mail.
     * @param home      L'emplacement de sa maison.
     * @param work      L'emplacement de son lieu de travail.
     */
    public User(String login, String password, String firstname, String lastname, String email, Location home,
                Location work) {
        this.login = login;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.home = home;
        this.work = work;
        this.allergies = new HashSet<>();
    }

    /**
     * Constructeur.
     *
     * @param login     Le login.
     * @param password  Le mot de passe.
     * @param firstname Le prénom.
     * @param lastname  Le nom.
     * @param email     L'adresse mail.
     */
    public User(String login, String password, String firstname, String lastname, String email) {
        this.login = login;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    /**
     * Getter pour le login.
     *
     * @return Le login.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Setter pour le login.
     *
     * @param login Le login.
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Getter pour le mot de passe.
     *
     * @return Le mot de passe.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter pour le mot de passe.
     *
     * @param password Le mot de passe.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter pour le prénom.
     *
     * @return Le prénom.
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Setter pour le prénom.
     *
     * @param firstname Le prénom.
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Getter pour le nom.
     *
     * @return Le nom.
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Setter pour le nom.
     *
     * @param lastname Le nom.
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Getter pour l'adresse mail.
     *
     * @return L'adresse mail.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter pour l'adresse mail.
     *
     * @param email L'adresse mail.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter pour l'emplacement de sa maison.
     *
     * @return L'emplacement de sa maison.
     */
    public Location getHome() {
        return home;
    }

    /**
     * Setter pour l'emplace de sa maison.
     *
     * @param home L'emplacement de sa maison.
     */
    public void setHome(Location home) {
        this.home = home;
    }

    /**
     * Getter pour l'emplacement du lieu de travail.
     *
     * @return L'emplacement du lieu de travail.
     */
    public Location getWork() {
        return work;
    }

    /**
     * Setter pour l'emplacement du lieu de travail.
     *
     * @param work L'emplacement du lieu de travail.
     */
    public void setWork(Location work) {
        this.work = work;
    }

    /**
     * Getter pour les allergies.
     *
     * @return Les allergies.
     */
    public Set<Allergy> getAllergies() {
        return allergies;
    }

    /**
     * Setter pour les allergies.
     *
     * @param allergies Les allergies.
     */
    public void setAllergies(Set<Allergy> allergies) {
        this.allergies = allergies;
    }

    /**
     * Ajouter une allergie.
     *
     * @param allergy L'allergie.
     */
    public void addAllergy(Allergy allergy) {
        this.allergies.add(allergy);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", home=" + home +
                ", work=" + work +
                '}';
    }
}