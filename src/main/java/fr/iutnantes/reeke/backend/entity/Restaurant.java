package fr.iutnantes.reeke.backend.entity;

import javax.persistence.*;
import java.util.*;

/**
 * @author JRoul
 * @version 0.1
 */
@Entity
@Table(name = "Restaurants")
public class Restaurant {

    /**
     * identifiant du restaurant
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * nom du restaurant
     */
    @Column(name = "name")
    private String name;

    /**
     * tel du restaurant
     */
    @Column(name = "tel")
    private String tel;

    /**
     * mail du restaurant
     */
    @Column(name = "mail")
    private String mail;

    /**
     * web du restaurant
     */
    @Column(name = "web")
    private String web;

    /**
     * note du restaurant
     */
    @Column(name = "note")
    private Double note;

    /**
     * lieux du restaurant
     */
    @OneToOne
    @JoinColumn(name = "place_id", referencedColumnName = "id")
    private Location place;

    /**
     * categorie du restaurant
     */
    @OneToOne
    @JoinColumn(name = "category_id", referencedColumnName = "name")
    private Category category;

    /**
     * images du restaurant
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "restaurants_images", joinColumns = @JoinColumn(name = "restaurant_id"), inverseJoinColumns = @JoinColumn(name = "image_id"))
    private Set<Image> images;

    /**
     * plats du restaurant
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "restaurants_dishes", joinColumns = @JoinColumn(name = "restaurant_id"), inverseJoinColumns = @JoinColumn(name = "dish_id"))
    private Set<Dish> dishes;

    /**
     * plats du restaurant
     */
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "restaurant_id")
    private List<OpenedTime> openedTimes;

    /**
     * constructeur vide d'un restaurant
     */
    public Restaurant() {
    }

    /**
     * constructeur d'un restaurant
     *
     * @param name     Le nom.
     * @param tel      Le numéro de téléphone.
     * @param mail     L'adresse mail.
     * @param web      L'URL du site web.
     * @param place    L'emplacement.
     * @param category La catégories.
     * @param note la note
     */
    public Restaurant(String name, String tel, String mail, String web, Location place, Category category, Double note) {
        this.name = name;
        this.tel = tel;
        this.mail = mail;
        this.web = web;
        this.place = place;
        this.category = category;
        this.images = new HashSet<>();
        this.dishes = new HashSet<>();
        this.openedTimes = new LinkedList<>();
        this.note = note;
    }

    /**
     * getter id du restaurant
     *
     * @return l'id
     */
    public Integer getId() {
        return id;
    }

    /**
     * getter nom du restaurant
     *
     * @return Le nom.
     */
    public String getName() {
        return name;
    }

    /**
     * setter nom du restaurant
     *
     * @param name Le nom.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter tel du restaurant
     *
     * @return Le numéro de téléphone.
     */
    public String getTel() {
        return tel;
    }

    /**
     * setter tel du restaurant
     *
     * @param tel Le numéro de téléphone.
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * getter mail du restaurant
     *
     * @return L'adresse mail.
     */
    public String getMail() {
        return mail;
    }

    /**
     * setter mail du restaurant
     *
     * @param mail L'adresse mail.
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * getter site web du restaurant
     *
     * @return L'URL du site web.
     */
    public String getWeb() {
        return web;
    }

    /**
     * setter site web du restaurant
     *
     * @param web L'URL du site web.
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * getter lieux du restaurant
     *
     * @return L'emplacement.
     */
    public Location getPlace() {
        return place;
    }

    /**
     * setter lieux du restaurant
     *
     * @param place L'emplacement.
     */
    public void setPlace(Location place) {
        this.place = place;
    }

    /**
     * getter category du restaurant
     *
     * @return La catégorie.
     */
    public Category getCategory() {
        return category;
    }

    /**
     * setter category du restaurant
     *
     * @param category La catégorie.
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * getter des images du restaurant
     *
     * @return Les images.
     */
    public Set<Image> getImages() {
        return images;
    }

    /**
     * setter des images du restaurants
     *
     * @param images Les images.
     */
    public void setImages(Set<Image> images) {
        this.images = images;
    }

    /**
     * getter des plats du restaurant
     *
     * @return Les plats.
     */
    public Set<Dish> getDishes() {
        return dishes;
    }

    /**
     * setter des plat du restaurant
     *
     * @param dishes Les plats.
     */
    public void setDishes(Set<Dish> dishes) {
        this.dishes = dishes;
    }

    /**
     * Ajouter un plat.
     *
     * @param dish Le plat.
     */
    public void addDish(Dish dish) {
        this.dishes.add(dish);
    }

    

    /**
     * Getter pour les horaires d'ouvertures.
     *
     * @return Les horaires d'ouvertures.
     */
    public List<OpenedTime> getOpenedTimes() {
        return openedTimes;
    }

    /**
     * Ajouter un horaire d'ouverture.
     *
     * @param openedTime l'horaire d'ouverture..
     */
    public void addOpenedTime(OpenedTime openedTime) {
        this.openedTimes.add(openedTime);
    }


    /**
     * Supprimer un plat d'un restaurant.
     *
     * @param dish Le plat.
     */
    public void removeDish(Dish dish) {
        this.dishes.remove(dish);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Restaurant)) return false;
        Restaurant that = (Restaurant) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", mail='" + mail + '\'' +
                ", web='" + web + '\'' +
                ", place=" + place +
                ", category=" + category +
                ", images=" + images +
                ", dishes=" + dishes +
                '}';
    }

    /**
     * getter note du restaurant
     * @return note
     */
    public Double getNote() {
        return note;
    }

    /**
     * setter note du restaurant
     * @param note
     */
    public void setNote(Double note) {
        this.note = note;
    }
}