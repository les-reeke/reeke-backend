package fr.iutnantes.reeke.backend.entity.note;

import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.Objects;

/**
 * Clé composite de la {@link Note}.
 *
 * @author JRoul
 * @version 0.1
 */
public class NoteId implements Serializable {

    /**
     * Identifiant serialisation
     */
    private static final long serialVersionUID = -3000285764693960840L;

    /**
     * identifiant reference user de la note
     */
    private String userId;

    /**
     * identifiant reference restaurant de la note
     */
    private Integer restaurantId;

    /**
     * identifiant reference dish de la note
     */
    @Null
    private Integer dishId;

    public NoteId() {
    }

    /**
     * Getter l'id de l'utilisateur.
     *
     * @return L'id de l'utilisateur.
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Setter l'id de l'utilisateur.
     *
     * @param userId L'id de l'utilisateur.
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Getter pour l'id du restaurant.
     *
     * @return L'id du restaurant.
     */
    public Integer getRestaurantId() {
        return restaurantId;
    }

    /**
     * Setter du restaurant.
     *
     * @param restaurantId L'id du restaurant.
     */
    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    /**
     * Getter pour l'id du plat.
     *
     * @return L'id du plat.
     */
    @Null
    public Integer getDishId() {
        return dishId;
    }

    /**
     * Setter pour l'id du plat.
     *
     * @param dishId L'id du plat.
     */
    public void setDishId(@Null Integer dishId) {
        this.dishId = dishId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NoteId)) return false;
        NoteId noteId = (NoteId) o;
        return Objects.equals(userId, noteId.userId) &&
                Objects.equals(restaurantId, noteId.restaurantId) &&
                Objects.equals(dishId, noteId.dishId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, restaurantId, dishId);
    }

    @Override
    public String toString() {
        return "NoteId{" +
                "userId='" + userId + '\'' +
                ", restaurantId=" + restaurantId +
                ", dishId=" + dishId +
                '}';
    }
}