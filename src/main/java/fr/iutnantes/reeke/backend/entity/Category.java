package fr.iutnantes.reeke.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author JRoul
 * @version 0.1
 */
@Entity
@Table(name = "categories")
public class Category {

    /**
     * identifiant de la categorie
     */
    @Id
    @Column(name = "name", unique = true)
    private String name;

    /**
     * constructeur vide d'une category
     */
    public Category() {
    }

    /**
     * constructeur d'une category
     *
     * @param name Le nom.
     */
    public Category(String name) {
        this.name = name;
    }

    /**
     * getter nom de la categorie
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter nom de la categorie
     * @param name Le nom.
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                '}';
    }
}