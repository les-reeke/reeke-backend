package fr.iutnantes.reeke.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author JRoul
 * @version 0.1
 */
@Entity
@Table(name = "allergies")
public class Allergy {

    /**
     * Nom de l'allergie
     */
    @Id
    @Column(name = "name")
    private String name;

    /**
     * Constructeur vide d'une allergie
     */
    public Allergy() {
    }

    /**
     * constructeur d'une allergie
     * @param name
     */
    public Allergy(String name) {
        this.name = name;
    }

    /**
     * getter nom de l'allergie
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter nom de l'allergie
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Allergy{" +
                "name='" + name + '\'' +
                '}';
    }
}