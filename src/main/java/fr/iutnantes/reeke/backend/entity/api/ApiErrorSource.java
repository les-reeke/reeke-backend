package fr.iutnantes.reeke.backend.entity.api;

/**
 * Source d'une erreur API.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ApiErrorSource {
    /**
     * Chemin de la source.
     */
    private String pointer;

    /**
     * Paramètre source vers l'erreur.
     */
    private String parameter;

    /**
     * Constructeur.
     *
     * @param pointer   Le chemin vers la source.
     * @param parameter Le paramètre source de l'erreur.
     */
    public ApiErrorSource(String pointer, String parameter) {
        this.pointer = pointer;
        this.parameter = parameter;
    }

    /**
     * Constructeur.
     *
     * @param pointer Le chemin vers la source.
     */
    public ApiErrorSource(String pointer) {
        this.pointer = pointer;
    }

    /**
     * Getter pour le chemin vers la source.
     *
     * @return Le chemin vers la source
     */
    public String getPointer() {
        return pointer;
    }

    /**
     * Setter pour le chemin vers la source.
     *
     * @param pointer Le chemin vers la source.
     */
    public void setPointer(String pointer) {
        this.pointer = pointer;
    }

    /**
     * Getter pour le paramètre source de l'erreur.
     *
     * @return Le paramètre source de l'erreur.
     */
    public String getParameter() {
        return parameter;
    }

    /**
     * Setter pour le paramètre source de l'erreur.
     *
     * @param parameter Le paramètre source de l'erreur.
     */
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public String toString() {
        return "ApiErrorSource{" +
                "pointer='" + pointer + '\'' +
                ", parameter='" + parameter + '\'' +
                '}';
    }
}
