package fr.iutnantes.reeke.backend.entity;

import javax.persistence.*;

/**
 * @author JRoul
 * @version 0.1
 */
@Entity
@Table(name = "images")
public class Image {

    /**
     * identifiant de l'image
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * url de l'image
     */
    @Column(name = "url")
    private String url;

    /**
     * constructeur vide d'une image
     */
    public Image() {
    }

    /**
     * constructeur d'un image
     *
     * @param url L'URL.
     */
    public Image(String url) {
        this.url = url;
    }

    /**
     * getter de l'url d'une image
     * @return url
     */
    public String getUrl() {
        return url;
    }

    /**
     * setter de l'url d'une image
     * @param url L'URL.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", url='" + url + '\'' +
                '}';
    }
}