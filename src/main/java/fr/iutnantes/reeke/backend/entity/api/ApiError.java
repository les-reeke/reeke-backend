package fr.iutnantes.reeke.backend.entity.api;

/**
 * Erreur d'appel de l'API.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ApiError {
    /**
     * Id.
     */
    private String id;

    /**
     * Lien vers le détail de l'erreur.
     */
    private String aboutLinks;

    /**
     * Status HTTP.
     */
    private String status;

    /**
     * Code d'erreur API.
     */
    private String code;

    /**
     * Titre de l'erreur.
     */
    private String title;

    /**
     * Détail de l'erreur.
     */
    private String detail;

    /**
     * Source de l'erreur.
     */
    private ApiErrorSource source;

    /**
     * Constructeur.
     *
     * @param id         L'id.
     * @param aboutLinks Lien vers le détail de l'erreur.
     * @param status     Le status HTTP.
     * @param code       Le code d'erreur API.
     * @param title      Le titre.
     * @param detail     Le détail.
     * @param source     La source.
     */
    public ApiError(String id, String aboutLinks, String status, String code, String title, String detail, ApiErrorSource source) {
        this.id = id;
        this.aboutLinks = aboutLinks;
        this.status = status;
        this.code = code;
        this.title = title;
        this.detail = detail;
        this.source = source;
    }

    /**
     * Getter pour l'id.
     *
     * @return L'id.
     */
    public String getId() {
        return id;
    }

    /**
     * Setter pour l'id.
     *
     * @param id L'id.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter pour le lien vers le détail de l'erreur.
     *
     * @return Le lien vers le détail de l'erreur.
     */
    public String getAboutLinks() {
        return aboutLinks;
    }

    /**
     * Setter pour le lien vers le détail de l'erreur.
     *
     * @param aboutLinks Le lien vers le détail de l'erreur.
     */
    public void setAboutLinks(String aboutLinks) {
        this.aboutLinks = aboutLinks;
    }

    /**
     * Getter pour le status HTTP.
     *
     * @return Le status HTTP.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter pour le status HTTP.
     *
     * @param status Le status HTTP.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Getter pour le code d'erreur API.
     *
     * @return Le code d'erreur API.
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter pour le code d'erreur API.
     *
     * @param code Le code d'erreur API.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Getter pour le titre.
     *
     * @return Le titre.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter pour le titre.
     *
     * @param title Le titre.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter pour le détail.
     *
     * @return Le détail.
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Setter pour le détail.
     *
     * @param detail Le détail.
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * Getter pour la source.
     *
     * @return La source.
     */
    public ApiErrorSource getSource() {
        return source;
    }

    /**
     * Setter pour la source.
     *
     * @param source La source.
     */
    public void setSource(ApiErrorSource source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "id='" + id + '\'' +
                ", aboutLinks='" + aboutLinks + '\'' +
                ", status='" + status + '\'' +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", detail='" + detail + '\'' +
                ", source=" + source +
                '}';
    }
}
