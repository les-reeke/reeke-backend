package fr.iutnantes.reeke.backend.entity;

import java.util.Date;

/**
 * Entité représentant un token.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class Token {
    /**
     * Id.
     */
    private Date id;

    /**
     * Token.
     */
    private String accessToken;

    /**
     * Temps avant expiration.
     */
    private long expiresIn;

    /**
     * Constructeur par défaut.
     */
    public Token() {
    }

    /**
     * Constructeur.
     *
     * @param id L'id.
     */
    public Token(Date id) {

    }

    /**
     * Constructeur.
     *
     * @param id          L'id.
     * @param accessToken Le token.
     * @param expiresIn   Le temps avant expiration.
     */
    public Token(Date id, String accessToken, long expiresIn) {
        this.id = id;
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
    }

    /**
     * Getter pour l'id.
     *
     * @return L'id.
     */
    public Date getId() {
        return id;
    }

    /**
     * Setter pour l'id.
     *
     * @param id L'id.
     */
    public void setId(Date id) {
        this.id = id;
    }

    /**
     * Getter pour le token.
     *
     * @return Le token.
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * Setter pour le token.
     *
     * @param accessToken Le token.
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * Getter pour le temps avant expiration.
     *
     * @return Le temps avant expiration
     */
    public long getExpiresIn() {
        return expiresIn;
    }

    /**
     * Setter pour le temps avant expiration.
     *
     * @param expiresIn Le temps avant expiration.
     */
    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", accessToken='" + accessToken + '\'' +
                ", expiresIn=" + expiresIn +
                '}';
    }
}
