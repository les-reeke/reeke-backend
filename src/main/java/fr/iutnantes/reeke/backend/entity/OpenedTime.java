package fr.iutnantes.reeke.backend.entity;

import javax.persistence.*;
import java.sql.Time;

/**
 * Entité representant les horaires d'ouverture.
 *
 * @author Jocelyn ROUL & Thibaut PICHON
 * @version 0.2
 */
@Entity
@Table(name = "openedtimes")
public class OpenedTime {

    /**
     * id de l'horaire
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * jours de l'horaire
     */
    @Column(name = "dayofweek")
    private Integer dayOfWeek;

    /**
     * debut de l'horaire
     */
    @Column(name = "starttime")
    private Time startTime;

    /**
     * fin de l'horaire
     */
    @Column(name = "endtime")
    private Time endTime;

    /**
     * constructeur vide de l'horaire
     */
    public OpenedTime() {
    }

    /**
     * constructeur de l'horaire
     *
     * @param dayOfWeek Le jour de la semaine (0-6 -> lundi-vendredi)
     * @param startTime L'heure de départ de la page horaire.
     * @param endTime   L'heure de fin de la page horaire.
     */
    public OpenedTime(Integer dayOfWeek, Time startTime, Time endTime) {
        this.dayOfWeek = dayOfWeek;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     * getter de l'id de l'horaire
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * getter du jour de l'horaire
     *
     * @return dayOfWeek
     */
    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * setter du jour de l'horaire
     *
     * @param dayOfWeek Le jour de la semaine (0-6 -> lundi-vendredi).
     */
    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * getter debut de l'horaire
     *
     * @return startTime L'heure de début de la plage horaire.
     */
    public Time getStartTime() {
        return startTime;
    }

    /**
     * setter debut de l'horaire
     *
     * @param startTime L'heure de début de la plage horaire.
     */
    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    /**
     * getter fin de l'horaire
     *
     * @return endTime L'heure de fin de la plage horaire.
     */
    public Time getEndTime() {
        return endTime;
    }

    /**
     * setter fin de l'horaire
     *
     * @param endTime L'heure de fin de la plage horaire.
     */
    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }


}