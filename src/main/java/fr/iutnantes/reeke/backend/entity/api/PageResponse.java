package fr.iutnantes.reeke.backend.entity.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Null;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Page de données.
 * <p>
 * <p>
 * |--------------|
 * |              |
 * |              |
 * |    EASTER    |
 * |              |
 * |     EGG      |
 * |              |
 * |         _____|
 * |         |   /
 * |         |  /
 * |---------|/
 *
 * @param <T> Le type.
 * @author Thibaut PICHON
 * @version 0.1
 * @see Page
 */
public class PageResponse<T> implements Page<T> {
    /**
     * La page.
     */
    private final Page<T> page;
    /**
     * L'URL.
     */
    private final String url;
    /**
     * La page rattachée.
     */
    @Null
    private PageResponse<?> related;

    /**
     * Constructeur pour les pages rattachées.
     *
     * @param page La page.
     * @param url  L'URL d'accès à la page.
     */
    public PageResponse(Page<T> page, String url) {
        this.page = page;
        this.url = url;
    }

    /**
     * Constructeur à partir du requête.
     *
     * @param page    La page.
     * @param request La requête.
     */
    public PageResponse(Page<T> page, HttpServletRequest request) {
        this.page = page;
        String url = request.getRequestURL().toString();
        if (request.getHeader("X-Forwarded-Proto") != null) {
            url = url.replace("http", request.getHeader("X-Forwarded-Proto"));
        }

        this.url = url + "?"
                + request.getParameterMap().keySet().stream()
                .map(key -> String.format("%s=%s", key, request.getParameter(key)))
                .collect(Collectors.joining("&"))
                .replace("&page", "page")
                .replaceAll("page\\[number\\]=[0-9]*", "")
                .replace("[", "%5B")
                .replace("]", "%5D");
    }

    /**
     * Getter pour la page rattachée.
     *
     * @return La page rattachée.
     */
    public Page<T> getPage() {
        return page;
    }

    /**
     * Getter pour l'URL de la page courante.
     *
     * @return L'URL de la page courante.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Getter pour l'URL vers la première page.
     *
     * @return L'URL vers la première page.
     */
    public String getFirstURL() {
        if (page.getTotalPages() == 0)
            return null;

        if (url.endsWith("?"))
            return url + String.format("page%%5Bnumber%%5D=%d", 0);
        else if (url.contains("?"))
            return url + String.format("&page%%5Bnumber%%5D=%d", 0);

        return url + String.format("?page%%5Bnumber%%5D=%d", 0);
    }

    /**
     * Getter pour l'URL vers la dernière page.
     *
     * @return L'ULR vers la dernière page.
     */
    public String getLastURL() {
        int last = page.getTotalPages() == 0 ? 0 : page.getTotalPages() - 1;

        if (last == 0)
            return null;

        if (url.endsWith("?"))
            return url + String.format("page%%5Bnumber%%5D=%d", last);
        else if (url.contains("?"))
            return url + String.format("&page%%5Bnumber%%5D=%d", last);

        return url + String.format("?page%%5Bnumber%%5D=%d", last);
    }

    /**
     * Getter pour l'URL vers la page suivante.
     *
     * @return L'URL vers la page suivante.
     */
    public String getNextURL() {
        if (!page.hasNext())
            return null;

        if (url.endsWith("?"))
            return url + String.format("page%%5Bnumber%%5D=%d", page.getNumber() + 1);
        else if (url.contains("?"))
            return url + String.format("&page%%5Bnumber%%5D=%d", page.getNumber() + 1);

        return url + String.format("?page%%5Bnumber%%5D=%d", page.getNumber() + 1);
    }

    /**
     * Getter pour l'URL vers la page précédente.
     *
     * @return L'URL vers la page précédente.
     */
    public String getPreviousURL() {
        if (!page.hasPrevious() || page.getTotalPages() == 0)
            return null;

        int previous = Math.min(page.getNumber() - 1, page.getTotalPages() - 1);

        if (url.endsWith("?"))
            return url + String.format("page%%5Bnumber%%5D=%d", previous);
        else if (url.contains("?"))
            return url + String.format("&page%%5Bnumber%%5D=%d", previous);

        return url + String.format("?page%%5Bnumber%%5D=%d", previous);
    }

    /**
     * Getter pour la page rattachée.
     *
     * @return La page rattachée.
     */
    @Null
    public PageResponse<?> getRelated() {
        return related;
    }

    /**
     * Setter pour la page rattachée.
     *
     * @param related La page rattachée.
     */
    public void setRelated(@Null PageResponse<?> related) {
        this.related = related;
    }

    @Override
    public int getTotalPages() {
        return page.getTotalPages();
    }

    @Override
    public long getTotalElements() {
        return page.getTotalElements();
    }

    @Override
    public int getNumber() {
        return page.getNumber();
    }

    @Override
    public int getSize() {
        return page.getSize();
    }

    @Override
    public int getNumberOfElements() {
        return page.getNumberOfElements();
    }

    @Override
    public List<T> getContent() {
        return page.getContent();
    }

    @Override
    public boolean hasContent() {
        return page.hasContent();
    }

    @Override
    public Sort getSort() {
        return page.getSort();
    }

    @Override
    public boolean isFirst() {
        return page.isFirst();
    }

    @Override
    public boolean isLast() {
        return page.isLast();
    }

    @Override
    public boolean hasNext() {
        return page.hasNext();
    }

    @Override
    public boolean hasPrevious() {
        return page.hasPrevious();
    }

    @Override
    public Pageable nextPageable() {
        return page.nextPageable();
    }

    @Override
    public Pageable previousPageable() {
        return page.previousPageable();
    }

    @Override
    public <U> Page<U> map(Function<? super T, ? extends U> converter) {
        return page.map(converter);
    }

    @Override
    public Iterator<T> iterator() {
        return page.iterator();
    }

    @Override
    public String toString() {
        return "PageResponse{" +
                "page=" + page +
                ", URL='" + url + '\'' +
                '}';
    }
}
