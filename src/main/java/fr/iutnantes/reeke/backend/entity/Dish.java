package fr.iutnantes.reeke.backend.entity;

import fr.iutnantes.reeke.backend.entity.note.Note;

import javax.persistence.*;
import java.util.*;

/**
 * @author JRoul
 * @version 0.1
 */
@Entity
@Table(name = "Dishes")
public class Dish {

    /**
     * identifiant du plat
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * nom du plat
     */
    @Column(name = "name")
    private String name;

    /**
     * description du plat
     */
    @Column(name = "description")
    private String description;

    /**
     * prix du plat
     */
    @Column(name = "price")
    private Double price;

    /**
     * Categorie du plat
     */
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "category_id", referencedColumnName = "name")
    private Category category;

    /**
     * images du plat
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "DISHES_IMAGES", joinColumns = @JoinColumn(name = "dish_id"), inverseJoinColumns = @JoinColumn(name = "image_id"))
    private Set<Image> images;

    /**
     * Les allergies.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ALLERGIES_DISHES", joinColumns = @JoinColumn(name = "dish_id"), inverseJoinColumns = @JoinColumn(name = "allergy_id"))
    private Set<Allergy> allergies;

    /**
     * Les notes.
     */
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "dish_id")
    private List<Note> notes;

    /**
     * La moyenne des notes.
     */
    @Column(name = "note")
    private Double note;

    /**
     * Les restaurant associé au plat.
     * <p>
     * Actuellement 1 seul restaurant.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "restaurants_dishes", joinColumns = @JoinColumn(name = "dish_id"), inverseJoinColumns = @JoinColumn(name = "restaurant_id"))
    private Set<Restaurant> restaurants;

    /**
     * constructeur vide d'un repas
     */
    public Dish() {
    }

    /**
     * constructeur d'un repas
     *
     * @param name        Le nom.
     * @param description La description.
     * @param price       La prix.
     * @param category    La catégorie.
     * @param images      Les images.
     */
    public Dish(String name, String description, double price, Category category, Set<Image> images) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
        this.images = images;
        this.notes = new LinkedList<>();
        this.images = new HashSet<>();
        this.allergies = new HashSet<>();
    }

    /**
     * constructeur d'un repas
     *
     * @param name        Le nom.
     * @param description La description.
     * @param price       La prix.
     * @param category    La catégorie.
     */
    public Dish(String name, String description, Double price, Category category) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
        this.notes = new LinkedList<>();
        this.images = new HashSet<>();
        this.allergies = new HashSet<>();
    }

    /**
     * Getter pour l'id.
     *
     * @return L'id.
     */
    public Integer getId() {
        return id;
    }

    /**
     * getter nom d'un repas
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * setter nom d'un repas
     *
     * @param name Le nom.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter description d'un repas
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * setter description d'un repas
     *
     * @param description La description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * getter prix d'un repas
     *
     * @return price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * setter pris d'un repas
     *
     * @param price Le prix.
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * getter category d'un repas
     *
     * @return category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * setter category d'un repas
     *
     * @param category La catégorie.
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * getter des images du plat
     *
     * @return images
     */
    public Set<Image> getImages() {
        return images;
    }

    /**
     * setter des images du plat
     *
     * @param images Les images.
     */
    public void setImages(Set<Image> images) {
        this.images = images;
    }

    /**
     * Getter pour les notes.
     *
     * @return Les notes.
     */
    public List<Note> getNotes() {
        return notes;
    }

    /**
     * Setter pour les notes.
     *
     * @param notes Les notes.
     */
    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    /**
     * Getter pour les allergies.
     *
     * @return Les allergies.
     */
    public Set<Allergy> getAllergies() {
        return allergies;
    }

    /**
     * Setter pour les allergies.
     *
     * @param allergies Les allergies.
     */
    public void setAllergies(Set<Allergy> allergies) {
        this.allergies = allergies;
    }

    /**
     * Ajout des allergies.
     *
     * @param allergies Les allergies.
     */
    public void addAllergies(Collection<Allergy> allergies) {
        this.allergies.addAll(allergies);
    }

    /**
     * Ajout d'une allergie.
     *
     * @param allergy L'allergie'.
     */
    public void addAllergy(Allergy allergy) {
        this.allergies.add(allergy);
    }

    /**
     * Ajouter une note.
     *
     * @param note La note.
     */
    public void addNote(Note note) {
        this.notes.add(note);
        this.computeNotesAverage();
    }

    /**
     * Ajouter une image.
     *
     * @param image L'image.
     */
    public void addImage(Image image) {
        this.images.add(image);
    }

    /**
     * Getter pour la moyenne des notes.
     *
     * @return La moyenne des notes.
     */
    public Double getNote() {
        return note;
    }

    /**
     * Getter pour les restaurants.
     *
     * @return Les restaurants.
     */
    public Set<Restaurant> getRestaurants() {
        return restaurants;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", category=" + category +
                ", images=" + images +
                ", allergies=" + allergies +
                ", notes=" + notes +
                '}';
    }

    @PostLoad
    public void onLoad() {
        this.computeNotesAverage();
    }

    /**
     * Mettre à jour la moyenne des notes.
     */
    public void computeNotesAverage() {
        OptionalDouble notesAverage = this.notes.stream().mapToDouble(Note::getNote).average();
        if (notesAverage.isPresent()) {
            this.note = notesAverage.getAsDouble();
        } else {
            this.note = 0d;
        }
    }

    /**
     * Setter pour l'id.
     *
     * @param id L'id.
     */
    public void setId(int id) {
        this.id = id;
    }
}