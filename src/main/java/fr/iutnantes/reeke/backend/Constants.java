package fr.iutnantes.reeke.backend;

import java.util.Collections;
import java.util.List;

/**
 * Constantes de l'application.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
public class Constants {
    /**
     * Constructeur par défaut.
     */
    private Constants() {
        // On cache le constructeur pour ne pas l'utiliser.
    }

    /*---------- Controlleurs constantes ---------*/
    public static final String TOKEN_PARAM = "token";
    public static final String LOGIN_PARAM = "login";
    public static final String PASSWORD_PARAM = "password";
    public static final String FIRSTNAME_PARAM = "firstname";
    public static final String LASTNAME_PARAM = "lastname";
    public static final String EMAIL_PARAM = "email";
    public static final String HOME_PARAM = "home";
    public static final String WORK_PARAM = "work";
    public static final String RESTAURANT_PARAM = "restaurant";
    public static final String NAME_PARAM = "name";
    public static final String CATEGORY_PARAM = "category";
    public static final String DESCRIPTION_PARAM = "description";
    public static final String NOTE_PARAM = "note";
    public static final String PRICE_PARAM = "price";
    public static final String ID_PARAM = "id";
    public static final String ALLERGIES_PARAM = "allergies";
    public static final String IMAGES_PARAM = "images";
    public static final String FILTER_RESTAURANT_PARAM = "filter[restaurant]";
    public static final String FILTER_RESTAURANT_ID = "filter[id]";
    public static final String FILTER_RESTAURANT_NAME = "filter[name]";
    public static final String FILTER_RESTAURANT_BEGIN = "filter[open-begin]";
    public static final String FILTER_RESTAURANT_END = "filter[open-end]";
    public static final String FILTER_RESTAURANT_NOTE = "filter[note]";
    public static final String FILTER_RESTAURANT_PLACE = "filter[place]";
    public static final String FILTER_RESTAURANT_DISH = "filter[dish]";
    public static final String FILTER_ALLERGIES_PARAM = "filter[allergies]";
    public static final String FILTER_NAME_PARAM = "filter[name]";
    public static final String PAGE_NUMBER_PARAM = "page[number]";
    public static final String PAGE_SIZE_PARAM = "page[size]";
    public static final String SORT_PARAM = "sort";

    /*---------- Services constantes ----------*/
    public static final List<String> SOURCES_LIST = Collections.singletonList("https://www.data.gouv.fr/fr/datasets/restaurants-3/");
    public static final String URL_AGENT_JSON = "https://www.data.gouv.fr/fr/datasets/r/8435b4d1-f328-422d-866b-58d2a1af813a";
}
