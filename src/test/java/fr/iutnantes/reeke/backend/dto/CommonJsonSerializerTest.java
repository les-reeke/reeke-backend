package fr.iutnantes.reeke.backend.dto;


import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.iutnantes.reeke.backend.Constants;
import fr.iutnantes.reeke.backend.entity.api.ApiError;
import fr.iutnantes.reeke.backend.entity.api.ApiErrorSource;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests de {@link CommonJsonSerializer}.
 *
 * @author Thibaut PICHON
 * @version 0.1
 */
class CommonJsonSerializerTest {
    /**
     * Test startSerializing - Cas de test n°1.
     *
     * @throws IOException Erreur d'entrée/sortie.
     */
    @Test
    void testStartSerializingCT1() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JsonGenerator gen = new JsonFactory().createGenerator(outputStream, JsonEncoding.UTF8);

        CommonJsonSerializer.startSerializing(gen);

        gen.close();

        TreeMap<String, Object> res = new ObjectMapper().readValue(new String(outputStream.toByteArray()), TreeMap.class);
        Map<String, String> jsonapiRes = (Map<String, String>) res.getOrDefault("jsonapi", new TreeMap<>());
        assertEquals("1.0", jsonapiRes.getOrDefault("version", null), "Vérification version JSON API");
    }

    /**
     * Test serializeErrors - Cas de test n°1.
     *
     * @throws IOException Erreur d'entrée/sortie.
     */
    @Test
    void testSerializeErrorsCT1() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JsonGenerator gen = new JsonFactory().createGenerator(outputStream, JsonEncoding.UTF8);
        gen.writeStartObject();

        ApiError apiError = mock(ApiError.class);
        ApiErrorSource apiErrorSource = mock(ApiErrorSource.class);
        when(apiError.getId()).thenReturn("1505");
        when(apiError.getAboutLinks()).thenReturn("https://google.fr");
        when(apiError.getCode()).thenReturn("1505");
        when(apiError.getDetail()).thenReturn("erreur de test");
        when(apiError.getStatus()).thenReturn("404 NOT FOUND");
        when(apiError.getTitle()).thenReturn("Test ERROR");
        when(apiError.getSource()).thenReturn(apiErrorSource);
        when(apiErrorSource.getPointer()).thenReturn("/user");
        when(apiErrorSource.getParameter()).thenReturn("login");

        CommonJsonSerializer.finishSerializing(gen, Collections.singletonList(apiError));

        gen.close();

        TreeMap<String, Object> res = new ObjectMapper().readValue(new String(outputStream.toByteArray()), TreeMap.class);

        List<Map<String, Object>> errors = (List<Map<String, Object>>) res.getOrDefault("errors", new ArrayList<>(0));
        assertEquals(1, errors.size(), "Vérification une seul erreur présente");
        Map<String, Object> error = errors.get(0);
        assertEquals("1505", error.getOrDefault("id", null), "Vérification id de l'erreur");
        assertEquals("1505", error.getOrDefault("code", null), "Vérification code de lerreur");
        assertEquals("erreur de test", error.getOrDefault("detail", null), "Vérification détail de lerreur");
        assertEquals("404 NOT FOUND", error.getOrDefault("status", null), "Vérification status HTTP de lerreur");
        assertEquals("Test ERROR", error.getOrDefault("title", null), "Vérification titre de l'erreur");
        Map<String, String> links = (Map<String, String>) error.getOrDefault("links", new TreeMap<>());
        assertEquals("https://google.fr", links.getOrDefault("about", null), "Vérification lien vers détail");
        Map<String, String> source = (Map<String, String>) error.getOrDefault("source", new TreeMap<>());
        assertEquals("/user", source.getOrDefault("pointer", null), "Vérification chemin de la source");
        assertEquals("login", source.getOrDefault("parameter", null), "Vérification paramètre de la source");
    }

    /**
     * Test finishSerializing - Cas de test n°1.
     *
     * @throws IOException Erreur d'entrée/sortie.
     */
    @Test
    void testFinishSerializingCT1() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JsonGenerator gen = new JsonFactory().createGenerator(outputStream, JsonEncoding.UTF8);

        gen.writeStartObject();

        CommonJsonSerializer.finishSerializing(gen);

        gen.close();

        TreeMap<String, Object> res = new ObjectMapper().readValue(new String(outputStream.toByteArray()), TreeMap.class);
        assertNull(res.getOrDefault("errors", new TreeMap<>()), "Vérification data non null");
        Map<String, Object> meta = (Map<String, Object>) res.getOrDefault("meta", new ArrayList<>(0));
        List<String> sources = (List<String>) meta.getOrDefault("sources", new ArrayList<>(0));
        assertTrue(sources.containsAll(Constants.SOURCES_LIST));
    }
}