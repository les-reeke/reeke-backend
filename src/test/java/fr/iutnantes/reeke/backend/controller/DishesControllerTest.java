package fr.iutnantes.reeke.backend.controller;

import fr.iutnantes.reeke.backend.entity.Category;
import fr.iutnantes.reeke.backend.entity.Dish;
import fr.iutnantes.reeke.backend.entity.Restaurant;
import fr.iutnantes.reeke.backend.entity.User;
import fr.iutnantes.reeke.backend.repository.*;
import fr.iutnantes.reeke.backend.util.TokenEngine;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests de {@link DishesController}.
 *
 * @author Thibaut PICHON.
 * @version 0.1
 * @see SpringBootTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@ActiveProfiles("test")
class DishesControllerTest {
    @Autowired
    private MockMvc mockMvc;

    /**
     * Le repository des restaurants.
     */
    @MockBean
    private RestaurantsRepository restaurantsRepository;
    /**
     * Le repository des plats.
     */
    @MockBean
    private DishesRepository dishesRepository;
    /**
     * Le repository des catégories.
     */
    @MockBean
    private CategoriesRepository categoriesRepository;
    /**
     * Le repository des notes.
     */
    @MockBean
    private NotesRepository notesRepository;
    /**
     * Repository des allergies.
     */
    @MockBean
    private AllergiesRepository allergiesRepository;
    /**
     * Repository des images.
     */
    @MockBean
    private ImagesRepository imagesRepository;
    /**
     * Gestionnaire des tokens.
     */
    @MockBean
    private TokenEngine tokenEngine;

    @Mock
    private User user;
    @Mock
    private Dish dish;
    @Mock
    private Dish dish2;
    @Mock
    private Category category;
    @Mock
    private Restaurant restaurant;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test tous les paramètres.
     * POST /restaurants/dishes
     */
    @Test
    public void testPostDishCT1() throws Exception {
        Mockito.when(restaurantsRepository.findById(anyInt())).thenReturn(Optional.of(restaurant));
        Mockito.when(tokenEngine.readToken(anyString())).thenReturn(Optional.of(user));
        Mockito.when(dish.getId()).thenReturn(1);
        Mockito.when(dish.getName()).thenReturn("plat");
        Mockito.when(dish.getCategory()).thenReturn(category);
        Mockito.when(dish.getAllergies()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getImages()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getNotes()).thenReturn(Collections.emptyList());
        Mockito.when(dish.getNote()).thenReturn(0d);
        Mockito.when(dish.getDescription()).thenReturn(null);
        Mockito.when(dish.getPrice()).thenReturn(null);
        Mockito.when(dishesRepository.save(any(Dish.class))).then(invocation -> {
            Dish dish = invocation.getArgument(0);
            dish.setId(1);
            return dish;
        });

        MvcResult mvcResult = mockMvc.perform(
                post("/restaurants/dishes")
                        .header("token", "bgkhb")
                        .header("restaurant", "1")
                        .header("name", "plat")
                        .header("category", "test"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))

                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data.id").value(1))
                .andExpect(jsonPath("$.data.type").value("dish"))
                .andExpect(jsonPath("$.data.attributes.name").value("plat"))
                .andExpect(jsonPath("$.data.attributes.description").isEmpty())
                .andExpect(jsonPath("$.data.attributes.category").value("test"))
                .andExpect(jsonPath("$.data.attributes.allergies").isEmpty())
                .andExpect(jsonPath("$.data.attributes.images").isEmpty())
                .andExpect(jsonPath("$.data.attributes.note").isEmpty())
                .andExpect(jsonPath("$.data.attributes.price").isEmpty())

                .andExpect(jsonPath("$.errors").isEmpty())
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test tous les paramètres manquants.
     * POST /restaurants/dishes
     */
    @Test
    public void testPostDishCT2() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/restaurants/dishes"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("400000"))
                .andExpect(jsonPath("$.errors[0].title").value("TOKEN REQUIRED"))
                .andExpect(jsonPath("$.errors[0].detail").value("Token attempted to be in request header"))
                .andExpect(jsonPath("$.errors[0].code").value("400000"))
                .andExpect(jsonPath("$.errors[0].status").value("400 BAD REQUEST"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("token"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test restaurant requis.
     * POST /restaurants/dishes
     */
    @Test
    public void testPostDishCT3() throws Exception {
        Mockito.when(tokenEngine.readToken("bgkhb")).thenReturn(Optional.empty());

        MvcResult mvcResult = mockMvc.perform(
                post("/restaurants/dishes")
                        .header("token", "bgkhb"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("400013"))
                .andExpect(jsonPath("$.errors[0].title").value("RESTAURANT REQUIRED"))
                .andExpect(jsonPath("$.errors[0].detail").value("Restaurant attempted to be in request header"))
                .andExpect(jsonPath("$.errors[0].code").value("400013"))
                .andExpect(jsonPath("$.errors[0].status").value("400 BAD REQUEST"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("restaurant"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test categorie requise.
     * POST /restaurants/dishes
     */
    @Test
    public void testPostDishCT4() throws Exception {
        Mockito.when(tokenEngine.readToken("bgkhb")).thenReturn(Optional.empty());

        MvcResult mvcResult = mockMvc.perform(
                post("/restaurants/dishes")
                        .header("token", "bgkhb")
                        .header("restaurant", "1"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("400014"))
                .andExpect(jsonPath("$.errors[0].title").value("CATEGORY REQUIRED"))
                .andExpect(jsonPath("$.errors[0].detail").value("Category attempted to be in request header"))
                .andExpect(jsonPath("$.errors[0].code").value("400014"))
                .andExpect(jsonPath("$.errors[0].status").value("400 BAD REQUEST"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("category"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test nom requis.
     * POST /restaurants/dishes
     */
    @Test
    public void testPostDishCT5() throws Exception {
        Mockito.when(tokenEngine.readToken("bgkhb")).thenReturn(Optional.empty());

        MvcResult mvcResult = mockMvc.perform(
                post("/restaurants/dishes")
                        .header("token", "bgkhb")
                        .header("restaurant", "1")
                        .header("category", "test"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("400008"))
                .andExpect(jsonPath("$.errors[0].title").value("NAME REQUIRED"))
                .andExpect(jsonPath("$.errors[0].detail").value("Name attempted to be in request header"))
                .andExpect(jsonPath("$.errors[0].code").value("400008"))
                .andExpect(jsonPath("$.errors[0].status").value("400 BAD REQUEST"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("name"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test token invalide.
     * POST /restaurants/dishes
     */
    @Test
    public void testPostDishCT6() throws Exception {
        Mockito.when(tokenEngine.readToken("bgkhb")).thenReturn(Optional.empty());

        MvcResult mvcResult = mockMvc.perform(
                post("/restaurants/dishes")
                        .header("token", "bgkhb")
                        .header("restaurant", "1")
                        .header("category", "test")
                        .header("name", "plat"))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("401001"))
                .andExpect(jsonPath("$.errors[0].title").value("INVALID TOKEN"))
                .andExpect(jsonPath("$.errors[0].detail").value("Token is invalid"))
                .andExpect(jsonPath("$.errors[0].code").value("401001"))
                .andExpect(jsonPath("$.errors[0].status").value("401 UNAUTHORIZED"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/401"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("token"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test sans filtre par restaurant.
     * GET /restaurants/dishes
     */
    @Test
    public void testGetDishCT1() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                get("/restaurants/dishes"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("400007"))
                .andExpect(jsonPath("$.errors[0].title").value("RESTAURANT REQUIRED"))
                .andExpect(jsonPath("$.errors[0].detail").value("Restaurant attempted to be in URL"))
                .andExpect(jsonPath("$.errors[0].code").value("400007"))
                .andExpect(jsonPath("$.errors[0].status").value("400 BAD REQUEST"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("filter[restaurant]"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test récupération avec succès.
     * GET /restaurants/dishes
     */
    @Test
    public void testGetDishCT2() throws Exception {
        Mockito.when(restaurantsRepository.findById(anyInt())).thenReturn(Optional.of(restaurant));
        Mockito.when(dish.getId()).thenReturn(1);
        Mockito.when(dish.getName()).thenReturn("plat");
        Mockito.when(dish.getCategory()).thenReturn(category);
        Mockito.when(dish.getAllergies()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getImages()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getNotes()).thenReturn(Collections.emptyList());
        Mockito.when(dish.getNote()).thenReturn(0d);
        Mockito.when(dish.getDescription()).thenReturn(null);
        Mockito.when(dish.getPrice()).thenReturn(null);
        Mockito.when(category.getName()).thenReturn("test");
        Mockito.when(dishesRepository.findByRestaurants(any(Restaurant.class), any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(dish), PageRequest.of(0, 20), 1));

        MvcResult mvcResult = mockMvc.perform(
                get("/restaurants/dishes")
                        .param("filter[restaurant]", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))

                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data[0].id").value(1))
                .andExpect(jsonPath("$.data[0].type").value("dish"))
                .andExpect(jsonPath("$.data[0].attributes.name").value("plat"))
                .andExpect(jsonPath("$.data[0].attributes.description").isEmpty())
                .andExpect(jsonPath("$.data[0].attributes.category").value("test"))
                .andExpect(jsonPath("$.data[0].attributes.allergies").isEmpty())
                .andExpect(jsonPath("$.data[0].attributes.images").isEmpty())
                .andExpect(jsonPath("$.data[0].attributes.note").value(0.0))
                .andExpect(jsonPath("$.data[0].attributes.price").isEmpty())

                .andExpect(jsonPath("$.errors").isEmpty())
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test token requis.
     * PUT /restaurants/dishes
     */
    @Test
    public void testPutDishCT1() throws Exception {
        Mockito.when(restaurantsRepository.findById(anyInt())).thenReturn(Optional.of(restaurant));
        Mockito.when(tokenEngine.readToken(anyString())).thenReturn(Optional.empty());
        Mockito.when(dish.getId()).thenReturn(1);
        Mockito.when(dish.getName()).thenReturn("plat");
        Mockito.when(dish.getCategory()).thenReturn(category);
        Mockito.when(dish.getAllergies()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getImages()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getNotes()).thenReturn(Collections.emptyList());
        Mockito.when(dish.getNote()).thenReturn(0d);
        Mockito.when(dish.getDescription()).thenReturn(null);
        Mockito.when(dish.getPrice()).thenReturn(null);
        Mockito.when(category.getName()).thenReturn("test");
        Mockito.when(dishesRepository.findById(anyInt())).thenReturn(Optional.of(dish));

        MvcResult mvcResult = mockMvc.perform(
                put("/restaurants/dishes")
                        .header("id", "1")
                        .header("restaurant", "1")
                        .header("name", "plat2")
                        .header("category", "cc"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("400000"))
                .andExpect(jsonPath("$.errors[0].title").value("TOKEN REQUIRED"))
                .andExpect(jsonPath("$.errors[0].detail").value("Token attempted to be in request header"))
                .andExpect(jsonPath("$.errors[0].code").value("400000"))
                .andExpect(jsonPath("$.errors[0].status").value("400 BAD REQUEST"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("token"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test token invalide.
     * PUT /restaurants/dishes
     */
    @Test
    public void testPutDishCT2() throws Exception {
        Mockito.when(restaurantsRepository.findById(anyInt())).thenReturn(Optional.of(restaurant));
        Mockito.when(tokenEngine.readToken(anyString())).thenReturn(Optional.empty());
        Mockito.when(dish.getId()).thenReturn(1);
        Mockito.when(dish.getName()).thenReturn("plat");
        Mockito.when(dish.getCategory()).thenReturn(category);
        Mockito.when(dish.getAllergies()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getImages()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getNotes()).thenReturn(Collections.emptyList());
        Mockito.when(dish.getNote()).thenReturn(0d);
        Mockito.when(dish.getDescription()).thenReturn(null);
        Mockito.when(dish.getPrice()).thenReturn(null);
        Mockito.when(category.getName()).thenReturn("test");
        Mockito.when(dishesRepository.findById(anyInt())).thenReturn(Optional.of(dish));

        MvcResult mvcResult = mockMvc.perform(
                put("/restaurants/dishes")
                        .header("token", "dvdsvsd")
                        .header("id", "1")
                        .header("restaurant", "1")
                        .header("name", "plat2")
                        .header("category", "cc"))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("401001"))
                .andExpect(jsonPath("$.errors[0].title").value("INVALID TOKEN"))
                .andExpect(jsonPath("$.errors[0].detail").value("Token is invalid"))
                .andExpect(jsonPath("$.errors[0].code").value("401001"))
                .andExpect(jsonPath("$.errors[0].status").value("401 UNAUTHORIZED"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/401"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("token"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test id requis.
     * PUT /restaurants/dishes
     */
    @Test
    public void testPutDishCT3() throws Exception {
        Mockito.when(restaurantsRepository.findById(anyInt())).thenReturn(Optional.of(restaurant));
        Mockito.when(tokenEngine.readToken(anyString())).thenReturn(Optional.of(user));
        Mockito.when(dish.getId()).thenReturn(1);
        Mockito.when(dish.getName()).thenReturn("plat");
        Mockito.when(dish.getCategory()).thenReturn(category);
        Mockito.when(dish.getAllergies()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getImages()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getNotes()).thenReturn(Collections.emptyList());
        Mockito.when(dish.getNote()).thenReturn(0d);
        Mockito.when(dish.getDescription()).thenReturn(null);
        Mockito.when(dish.getPrice()).thenReturn(null);
        Mockito.when(category.getName()).thenReturn("test");
        Mockito.when(dishesRepository.findById(anyInt())).thenReturn(Optional.of(dish));

        MvcResult mvcResult = mockMvc.perform(
                put("/restaurants/dishes")
                        .header("token", "dvdsvsd")
                        .header("restaurant", "1")
                        .header("name", "plat2")
                        .header("category", "cc"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("400006"))
                .andExpect(jsonPath("$.errors[0].title").value("DEPENDENT PARAMETER REQUIRED"))
                .andExpect(jsonPath("$.errors[0].detail").value("id need undefined to be given"))
                .andExpect(jsonPath("$.errors[0].code").value("400006"))
                .andExpect(jsonPath("$.errors[0].status").value("400 BAD REQUEST"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("id"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test tous les paramètres.
     * PUT /restaurants/dishes
     */
    @Test
    public void testPutDishCT4() throws Exception {
        Mockito.when(restaurantsRepository.findById(anyInt())).thenReturn(Optional.of(restaurant));
        Mockito.when(tokenEngine.readToken(anyString())).thenReturn(Optional.of(user));
        Mockito.when(dish.getId()).thenReturn(1);
        Mockito.when(dish.getName()).thenReturn("plat");
        Mockito.when(dish.getCategory()).thenReturn(category);
        Mockito.when(dish.getAllergies()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getImages()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getNotes()).thenReturn(Collections.emptyList());
        Mockito.when(dish.getNote()).thenReturn(0d);
        Mockito.when(dish.getDescription()).thenReturn(null);
        Mockito.when(dish.getPrice()).thenReturn(null);
        Mockito.when(category.getName()).thenReturn("test");
        Mockito.when(dishesRepository.findById(anyInt())).thenReturn(Optional.of(dish));

        MvcResult mvcResult = mockMvc.perform(
                put("/restaurants/dishes")
                        .header("token", "bgkhb")
                        .header("id", "1")
                        .header("restaurant", "1")
                        .header("name", "plat2")
                        .header("category", "cc"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))

                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data.id").value(1))
                .andExpect(jsonPath("$.data.type").value("dish"))
                .andExpect(jsonPath("$.data.attributes.name").value("plat"))
                .andExpect(jsonPath("$.data.attributes.description").isEmpty())
                .andExpect(jsonPath("$.data.attributes.category").value("test"))
                .andExpect(jsonPath("$.data.attributes.allergies").isEmpty())
                .andExpect(jsonPath("$.data.attributes.images").isEmpty())
                .andExpect(jsonPath("$.data.attributes.note").value(0.0))
                .andExpect(jsonPath("$.data.attributes.price").isEmpty())

                .andExpect(jsonPath("$.errors").isEmpty())
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
        Mockito.verify(dish, Mockito.times(1)).setName("plat2");
        Mockito.verify(dish, Mockito.times(1)).setCategory(any(Category.class));
    }

    /**
     * Test id requis.
     * DELETE /restaurants/dishes
     */
    @Test
    public void testDeleteDishCT1() throws Exception {
        Mockito.when(restaurantsRepository.findById(anyInt())).thenReturn(Optional.of(restaurant));
        Mockito.when(tokenEngine.readToken(anyString())).thenReturn(Optional.of(user));
        Mockito.when(dish.getId()).thenReturn(1);
        Mockito.when(dish.getName()).thenReturn("plat");
        Mockito.when(dish.getCategory()).thenReturn(category);
        Mockito.when(dish.getAllergies()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getImages()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getNotes()).thenReturn(Collections.emptyList());
        Mockito.when(dish.getNote()).thenReturn(0d);
        Mockito.when(dish.getDescription()).thenReturn(null);
        Mockito.when(dish.getPrice()).thenReturn(null);
        Mockito.when(category.getName()).thenReturn("test");
        Mockito.when(dishesRepository.findById(anyInt())).thenReturn(Optional.of(dish));

        MvcResult mvcResult = mockMvc.perform(
                delete("/restaurants/dishes")
                        .header("token", "dvdsvsd"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))
                .andExpect(jsonPath("$.data").isEmpty())

                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].id").value("400006"))
                .andExpect(jsonPath("$.errors[0].title").value("DEPENDENT PARAMETER REQUIRED"))
                .andExpect(jsonPath("$.errors[0].detail").value("id need undefined to be given"))
                .andExpect(jsonPath("$.errors[0].code").value("400006"))
                .andExpect(jsonPath("$.errors[0].status").value("400 BAD REQUEST"))

                .andExpect(jsonPath("$.errors[0].links").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].links.about").value("https://developer.mozilla.org/fr/docs/Web/HTTP/Status/400"))

                .andExpect(jsonPath("$.errors[0].source").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].source.pointer").value("/restaurants/dishes"))
                .andExpect(jsonPath("$.errors[0].source.parameter").value("name"))
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
    }

    /**
     * Test tous les paramètres.
     * DELETE /restaurants/dishes
     */
    @Test
    public void testDeleteDishCT2() throws Exception {
        Mockito.when(restaurantsRepository.findById(anyInt())).thenReturn(Optional.of(restaurant));
        Mockito.when(tokenEngine.readToken(anyString())).thenReturn(Optional.of(user));
        Mockito.when(dish.getId()).thenReturn(1);
        Mockito.when(dish.getName()).thenReturn("plat");
        Mockito.when(dish.getCategory()).thenReturn(category);
        Mockito.when(dish.getAllergies()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getImages()).thenReturn(Collections.emptySet());
        Mockito.when(dish.getNotes()).thenReturn(Collections.emptyList());
        Mockito.when(dish.getNote()).thenReturn(0d);
        Mockito.when(dish.getDescription()).thenReturn(null);
        Mockito.when(dish.getPrice()).thenReturn(null);
        Mockito.when(category.getName()).thenReturn("test");
        Mockito.when(dishesRepository.findById(anyInt())).thenReturn(Optional.of(dish));

        MvcResult mvcResult = mockMvc.perform(
                delete("/restaurants/dishes")
                        .header("token", "bgkhb")
                        .param("id", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.jsonapi").isNotEmpty())
                .andExpect(jsonPath("$.jsonapi.version").value("1.0"))

                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data.id").value(1))
                .andExpect(jsonPath("$.data.type").value("dish"))
                .andExpect(jsonPath("$.data.attributes.name").value("plat"))
                .andExpect(jsonPath("$.data.attributes.description").isEmpty())
                .andExpect(jsonPath("$.data.attributes.category").value("test"))
                .andExpect(jsonPath("$.data.attributes.allergies").isEmpty())
                .andExpect(jsonPath("$.data.attributes.images").isEmpty())
                .andExpect(jsonPath("$.data.attributes.note").value(0.0))
                .andExpect(jsonPath("$.data.attributes.price").isEmpty())

                .andExpect(jsonPath("$.errors").isEmpty())
                .andReturn();

        Assert.assertEquals("application/json;charset=UTF-8",
                mvcResult.getResponse().getContentType());
        Mockito.verify(dishesRepository, Mockito.times(1)).delete(any(Dish.class));
    }
}